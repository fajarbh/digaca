<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Welcome';

//Default Controller Admin
$route['Admin'] = 'Admin/AdminController';

//Controller LoginAdmin
$route['Admin/login'] = 'Admin/LoginadminController';
$route['Admin/logout'] = 'Admin/LoginadminController/logout';
$route['Admin/proseslogin'] = 'Admin/LoginadminController/login';
$route['Admin/register'] = 'Admin/LoginadminController/check_register';
$route['Admin/export'] = 'Admin/KelasController/export_excel';
$route['Admin/import'] = 'Admin/MuridController/saveimport';

//Dashboard Admin
$route['Admin/kelas'] = 'Admin/AdminController/kelas';
$route['Admin/guru'] = 'Admin/AdminController/guru';
$route['Admin/mapel'] = 'Admin/AdminController/mapel';
$route['Admin/murid'] = 'Admin/AdminController/murid';
$route['Admin/akunadmin'] = 'Admin/AdminController/akunadmin';
$route['Admin/akunguru'] = 'Admin/AdminController/akunguru';
$route['Admin/akunwalikelas'] = 'Admin/AdminController/akunwalikelas';
$route['Admin/akunsiswa'] = 'Admin/AdminController/akunsiswa';

//Controller Kelas
$route['Admin/kelas/kelas_list_json'] = 'Admin/KelasController/kelas_list_json';
$route['Admin/kelas/add_json'] = 'Admin/KelasController/add_json';
$route['Admin/kelas/edit_json'] = 'Admin/KelasController/edit_json';
$route['Admin/kelas/delete_json'] = 'Admin/KelasController/delete_json';
$route['Admin/kelas/get_by_id/(:num)'] = 'Admin/KelasController/get_by_id/$1';
//Controler Murid
$route['Admin/murid/murid_list_json'] = 'Admin/MuridController/murid_list_json';
$route['Admin/murid/add_json'] = 'Admin/MuridController/add_json';
$route['Admin/murid/edit_json'] = 'Admin/MuridController/edit_json';
$route['Admin/murid/delete_json'] = 'Admin/MuridController/delete_json';
$route['Admin/murid/get_by_id/(:num)'] = 'Admin/MuridController/get_by_id/$1';

//Controller Guru
$route['Admin/guru/guru_list_json'] = 'Admin/GuruController/guru_list_json';
$route['Admin/guru/add_json'] = 'Admin/GuruController/add_json';
$route['Admin/guru/delete_json'] = 'Admin/GuruController/delete_json';
$route['Admin/guru/edit_json'] = 'Admin/GuruController/edit_json';
$route['Admin/guru/get_by_id/(:num)'] = 'Admin/GuruController/get_by_id/$1';

//Controller Mapel
$route['Admin/mapel/mapel_list_json'] = 'Admin/MapelController/mapel_list_json';
$route['Admin/mapel/add_json'] = 'Admin/MapelController/add_json';
$route['Admin/mapel/delete_json'] = 'Admin/MapelController/delete_json';
$route['Admin/mapel/edit_json'] = 'Admin/MapelController/edit_json';
$route['Admin/mapel/get_by_id/(:num)'] = 'Admin/MapelController/get_by_id/$1';

//Controller User
$route['Admin/user/user_list_json'] = 'Admin/UserController/user_list_json';
$route['Admin/user/add_json'] = 'Admin/UserController/add_json';
$route['Admin/user/edit_json'] = 'Admin/UserController/edit_json';
$route['Admin/user/get_by_id/(:num)'] = 'Admin/UserController/get_by_id/$1';

//Login Guru
$route['Admin/akunadmin/akun_list_json'] = 'Admin/AkunController/akun_list_json';
$route['Admin/akunadmin/add_json'] = 'Admin/AkunController/add_json';
$route['Admin/akunadmin/edit_json'] = 'Admin/AkunController/edit_json';
$route['Admin/akunadmin/delete_json'] = 'Admin/AkunController/delete_json';
$route['Admin/akunadmin/get_by_id/(:num)'] = 'Admin/AkunController/get_by_id/$1';


//Controller guru
$route['Guru'] = 'Guru/GuruController';
$route['Guru/Jadwalpelajaran'] = 'Guru/GuruController/jadwalpelajaran';
$route['Guru/Nilaisiswa'] = 'Guru/GuruController/nilaisiswa';

//Cotroller Nilaisiswa
$route['Guru/Nilaisiswa/get_subnama'] = 'Guru/GuruController/get_subnama';
$route['Guru/Nilaisiswa/nilai_list_json'] = 'Guru/NilaiController/nilai_list_json';
$route['Guru/Nilaisiswa/add_json'] = 'Guru/NilaiController/add_json';
$route['Guru/Nilaisiswa/edit_json'] = 'Guru/NilaiController/edit_json';
$route['Guru/Nilaisiswa/delete_json'] = 'Guru/NilaiController/delete_json';
$route['Guru/Nilaisiswa/get_by_id/(:num)'] = 'Guru/NilaiController/get_by_id/$1';

//Controller Oauth
$route['Siswa/login'] = 'Siswa/AuthController';
$route['Siswa/proseslogin'] = 'Siswa/AuthController/login';
$route['Siswa/logout'] = 'Siswa/SiswaController/logout';
$route['Siswa'] = 'Siswa/SiswaController';


//Controller Auth
$route['Siswa/register'] = 'Siswa/AuthController/check_register';
$route['Siswa/verifikasi'] = 'Siswa/AuthController/verify';
$route['Siswa/forgotpassword'] = 'Siswa/AuthController/forgotpassword';
$route['Siswa/changepassword'] = 'Siswa/AuthController/changepassword';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
