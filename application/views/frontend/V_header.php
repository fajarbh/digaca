<style type="text/css" media="screen">
    .text-donker{
     color:#024771;
     font-weight: bold;
    }
</style>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Digaca</title>
    <meta name="description" content="Responsive, Bootstrap, BS4">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="<?php echo config_item('images');?>logo.svg">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="<?php echo config_item('images');?>logo.svg">
    <link href="<?php echo config_item('css');?>modal-hapus.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo config_item('css');?>bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo config_item('asset');?>libs/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>app.css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>coremain.css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>daygridmain.css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>timegridmain.css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>listmain.css">
    <link rel="stylesheet" href="<?php echo config_item('css');?>theme/primary.css">
    <link rel="stylesheet" type="text/css" href="<?php echo config_item('asset');?>libs/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <script src="<?php echo config_item('js');?>jquery.min.js"></script>
    <script src="<?php echo config_item('js');?>Chart.js"></script>
    <script src="<?php echo config_item('js');?>coremain.js"></script>
    <script src="<?php echo config_item('js');?>daygridmain.js"></script>
    <script src="<?php echo config_item('js');?>timegridmain.js"></script>
    <script src="<?php echo config_item('js');?>listmain.js"></script>
    <script src="<?php echo config_item('js');?>sweetalert2.all.min.js"></script>
    <script src="<?php echo config_item('js');?>sweetalert2.min.js"></script>
    <link rel="manifest" href="<?= base_url();?>manifest.json">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
      <script type="text/javascript">
            
            function ClearFormData(id){
                 $(id)[0].reset();
            }

            function notif(_type_, _pesan_){ 
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 1750, 
                })
                Toast.fire({
                    type: _type_, 
                    title: _pesan_, 
                }); 
            }
    </script>

    <div class="app" id="app">
        <div id="aside" class="app-aside fade nav-expand bg-white" aria-hidden="true">
            <div class="sidenav modal-dialog">
                <div class="navbar box-shadow-2" style="background: #f8f8f8;">
                    <a href="index.html" class="navbar-brand"><span class="hidden-folded d-inline text-donker">DIGACA</span></a></div>
                <div
                    class="flex hide-scroll white bg-white text-donker" >
                    <div class="scroll">
                        <div class="nav-active-theme" data-nav>
                            <ul class="nav">
                                <li class="nav-header "><span class="text-xs hidden-folded">Main</span></li>
                                <li><a href="<?php echo site_url('Admin') ?>" ><span class="nav-icon"><i class="fa fa-dashboard"></i></span> <span class="nav-text" >Dashboard</span></a></li>
                                <li><a><span class="nav-caret"><i class="fa fa-caret-down"></i></span> <span class="nav-icon"><i class="fa fa-database"></i></span> <span class="nav-text">Master Data</span></a>
                                    <ul class="nav-sub">
                                        <li><a href="<?php echo site_url('Admin/kelas')?>" onclick="reload_table()"><span class="nav-text">Kelas</span></a></li>
                                        <li><a href="<?php echo site_url('Admin/murid')?>"><span class="nav-text">Murid</span></a></li>
                                        <li><a href="<?php echo site_url('Admin/mapel')?>"><span class="nav-text">Mapel</span></a></li>
                                    </ul>
                                </li>
                                <div class="add-to">
                                    <button class="add-to-btn">Add to home scrin</button>
                                </div>
                                <li class="pb-2 hidden-folded"></li>
                            </ul>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div id="content" class="app-content box-shadow-1 box-radius-1 " role="main">
        <div class="content-header text-white" id="content-header box-shadow-2" style="background: rgba(16,92,173,1);">
            <div class="navbar navbar-expand-lg"><a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg></a>
                <div class="navbar-text nav-title flex "
                    id="pageTitle" style="font-weight: bold;font-size: 18px"><?php echo $nav_title ?></div>
                <ul class="nav flex-row order-lg-2">
                    <li class="nav-item dropdown"><a class="nav-link px-3" data-toggle="dropdown"><i class="fa fa-bell text-muted"></i> <span class="badge badge-pill up danger"></span></a>
                        <div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
                            <div class="scrollable hover" style="max-height: 250px">
                                
                            </div>
                            
                        </div>
                    </li>
                    <li class="dropdown d-flex align-items-center"><a href="#" data-toggle="dropdown" class="d-flex align-items-center"><span class="avatar w-32"><img src="<?php echo config_item('images');?>a9.jpg" alt="..."></span></a>
                        <div class="dropdown-menu dropdown-menu-right w pt-0 mt-2 animate fadeIn">
                            <a class="dropdown-item" href="profile.html"><span>Profile</span> </a><a class="dropdown-item" href="setting.html"><span>Settings</span> </a><a class="dropdown-item" href="app.inbox.html"><span class="float-right"><span class="badge info">6</span></span> <span>Inbox</span> </a>
                            <a
                                class="dropdown-item" href="app.message.html"><span>Message</span></a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="docs.html">Need help? </a>
                                <a class="dropdown-item" href="<?php echo site_url('Admin/logout')?>">Sign out</a></div>
                    </li>
                </ul>
            </div>
        </div>
        
        
    <div id="setting" class="">
        <div class="setting dark-white rounded-bottom" id="theme"><a href="#" data-toggle-class="active" data-target="#theme" class="dark-white toggle"><i class="fa fa-gear text-primary fa-spin"></i></a>
            <div class="box-header"><strong>Theme Switcher</strong></div>
            <div class="box-divider"></div>
            <div class="box-body">
                <p id="settingLayout"><label class="md-check my-1 d-block"><input type="checkbox" name="fixedAside"> <i></i> <span>Fixed Aside</span></label><label class="md-check my-1 d-block"><input type="checkbox" name="fixedContent"> <i></i> <span>Fixed Content</span></label>
                    <label
                        class="md-check my-1 d-block"><input type="checkbox" name="folded"> <i></i> <span>Folded Aside</span></label><label class="md-check my-1 d-block"><input type="checkbox" name="ajax"> <i></i> <span>Ajax load page</span></label>
                </p>
        </div>
    </div>
    </div>
    