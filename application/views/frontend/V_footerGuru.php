    
    <script src="<?php echo config_item('js');?>app.js"></script>       
    <script src="<?php echo config_item('js');?>popper.js"></script>
    <script src="<?php echo config_item('asset');?>libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo config_item('asset');?>libs/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo config_item('js');?>bootstrap-datepicker.js"></script>	
    <script src="<?php echo config_item('js');?>Chart.js"></script>
    <script src="<?php echo config_item('js');?>coremain.js"></script>
    <script src="<?php echo config_item('js');?>daygridmain.js"></script>
    <script src="<?php echo config_item('js');?>timegridmain.js"></script>
    <script src="<?php echo config_item('js');?>listmain.js"></script>
    <script src="<?php echo config_item('js');?>sweetalert2.all.min.js"></script>
    <script src="<?php echo config_item('js');?>sweetalert2.min.js"></script>
    <script>
      $('#datepicker').datepicker({
       format: 'mm/dd/yyyy',
       autoclose: true,
       todayHighlight: true
   });

</script>
<script type="text/javascript">
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
  }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
  });
    });
  }

  let deferredPrompt;
  var div = document.querySelector('.add-to');
  var button = document.querySelector('.add-to-btn');
  /*div.style.display = 'none';*/
  window.addEventListener('beforeinstallprompt', (e) => {
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  div.style.display = 'block';

  button.addEventListener('click', (e) => {
  // hide our user interface that shows our A2HS button
  div.style.display = 'none';
  // Show the prompt
  deferredPrompt.prompt();
  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
  .then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
    } else {
        console.log('User dismissed the A2HS prompt');
    }
    deferredPrompt = null;
    });
    });

})
</script>
</body>
</html>