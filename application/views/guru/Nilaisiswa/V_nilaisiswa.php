<div class="content-main" id="content-main">
  <div class="padding">
    <!--<button type="" onclick="reload_tabel()">Reload</button> -->
    <button type="button" class="btn text-white btn-sm" style="background-color:#105cad;" id="tambah">Tambah Nilai Siswa</button>
    <div class="btn-group" role="group">
      <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle btn-sm text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Excel
      </button>
      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>">Export</a>
        <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>">Import</a>
      </div>
    </div>
    
    <div class="table-responsive">
      <br>
      <table id="datanilai" class="table v-middle p-0 m-0 box" data-plugin="dataTable" style="font-size: 14px">
        <thead>
          <tr>
            <th>No</th>            
            <th>Nama Siswa</th>
            <th>Mata Pelajaran</th>
            <th>Tahun Ajaran</th>
            <th>UTS</th>
            <th>UAS</th>
            <th>Pengetahuan</th>
            <th>Keterampilan</th>
            <th>KKM</th>
            <th>Semester</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
</div>
</div>


<!-- Modal tambah nilai -->
<div class="modal fade" id="ModalNilai" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formTambah">
                    <h4 class="text-center text-dark" style="padding-bottom: 15px">Tambah Nilai Siswa</h4>
                    <div class="row">
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">
                            <label class="form-check-label" style="font-size: 13px;color:grey;">Kelas</label>
                            <select name="add_kelas" class="form-control" id="nama" required="">
                                <option value="">-- PILIH --</option>
                                <?php foreach($data->result() as $row):?>
                                <option value="<?php echo $row->kelas;?>"><?php echo $row->kelas;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>                        
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">
                          <label class="form-check-label" style="font-size: 13px;color:grey;">Nama Siswa</label>
                            <select name="add_namasiswa" class="namasiswa form-control" required=""  id="select2-single" data-plugin="select2">
                                <option value="">-- PILIH --</option>                              
                            </select>
                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">
                            <label class="form-check-label" style="font-size: 13px;color:grey;">Mata pelajaran</label>
                            <select name="add_kode_mapel" class="form-control" id="nama">
                                <option value="">-- PILIH --</option>
                                <?php foreach($kode_mapel->result() as $row):?>
                                <option value="<?php echo $row->kode_mapel;?>"><?php echo $row->nama_mapel;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Tahun Ajaran</label>
                            <select name="add_thn_ajaran" class="form-control">
                                <option value="">-- PILIH --</option>
                                <?php foreach ($thn_ajaran as $data) {
                                    $id_ajrn=$data->id_semester;
                                    $nm_ajrn=$data->tahun_ajaran;
                                    echo "<option value='$nm_ajrn' selected>$nm_ajrn</option>";
                                }
                                ?>
                            </select>

                        </div>                                               
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai UTS</label>
                            <input class="form-control" type="text" required="" name="add_nilai_uts">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai UAS</label>
                            <input class="form-control" type="text" name="add_nilai_uas" required="">

                        </div>
                        <!-- ========================================================================================= -->
                          <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai Pengetahuan</label>
                            <input class="form-control" type="text" name="add_nilai_pengetahuan" required="">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">
                            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai Keterampilan</label>
                            <input class="form-control" type="text" name="add_nilai_keterampilan" required="">
                        </div>
                        <!-- ========================================================================================= -->
                          <div class="form-group col-md-6">
                            <label class="form-check-label" style="font-size: 13px;color:grey;">KKM</label>
                            <input class="form-control" type="text" name="add_kkm" required="">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">
                            <label class="form-check-label" style="font-size: 13px;color:grey;">Semester</label>
                            <select name="add_semester" class="form-control">
                                <option value="">-- PILIH --</option>
                                <?php foreach ($semester as $data) {
                                    $semester_id=$data->id_semester;
                                    $nm_smstr=$data->nama_semester;
                                    echo "<option value='$semester_id' selected>$nm_smstr</option>";
                                }
                                ?>
                            </select>

                        </div>
                        <!-- ========================================================================================= -->                        
                    </div>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-lg-3">
                          <button type="submit" class="btn btn-sm btn-primary text-white" id="btntambah">Tambah</button>
                          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--   End Modal -->

<!-- Modal Mapel Edit -->
<div class="modal fade" id="ModalNilaiedit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg  modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <form id="formEdit">
          <h4 class="text-center text-dark" style="padding-bottom: 15px;">Edit Nilai Siswa</h4>
          <div class="row">                      
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">Nama Siswa</label>
            <input type="hidden" name="edit_id">
            <input class="form-control" type="text" required="" name="edit_nama" readonly>
          </div>            
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">Mata Pelajaran</label>
            <input type="hidden" name="id">
            <input class="form-control" type="text" required="" name="edit_kode" readonly>
          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">Tahun Ajaran</label>
            <input class="form-control" type="text" required="" name="edit_thn_ajaran" readonly>
          </div>                                               
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">

            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai UTS</label>
            <input class="form-control" type="text" required="" name="edit_uts">

          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">

            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai UAS</label>
            <input class="form-control" type="text" name="edit_uas" required="">

          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">

            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai Pengetahuan</label>
            <input class="form-control" type="text" name="edit_pengetahuan" required="">

          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">Nilai Keterampilan</label>
            <input class="form-control" type="text" name="edit_keterampilan" required="">
          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">KKM</label>
            <input class="form-control" type="text" name="edit_kkm" required="" readonly>

          </div>
          <!-- ========================================================================================= -->
          <div class="form-group col-md-6">
            <label class="form-check-label" style="font-size: 13px;color:grey;">Semester</label>
            <input class="form-control" type="text" required="" name="edit_semester" readonly>
          </div>
          <!-- ========================================================================================= -->                         
        </div>
          <br>
          <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3">              
              <button type="submit" style="color:#105cad;" class="btn btn-primary btn-sm text-white" id="btnedit">Edit</button>
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--   End Modal -->

<!-- Hapus Nilai -->
<div id="ModalHapus" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE5CD;</i>
        </div>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_id" id="nilai_delete_id">
        <p>Apakah anda yakin data ingin di hapus?</p>
      </div>
      <div class="model-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" id="btndelete">Delete</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Hapus -->