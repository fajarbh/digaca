<script>


    var table;

    $(document).ready(function() {
        table = $('#datanilai').DataTable({
        "processing": false, //Feature control the processing indicator.
        ajax: "<?php echo site_url('Guru/Nilaisiswa/nilai_list_json')?>",
        stateSave : true,
        "order": [], //Initial no order.

    });
    });

    function edit_nilai(id)
    {
        $('#formEdit')[0].reset(); // reset form on modals
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('Guru/Nilaisiswa/get_by_id')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(res)
            {

                $('[name="edit_id"]').val(res.id);
                $('[name="edit_nama"]').val(res.nama_murid);
                $('[name="edit_kode"]').val(res.nama_mapel);
                $('[name="edit_thn_ajaran"]').val(res.thn_ajaran);
                $('[name="edit_uts"]').val(res.uts);
                $('[name="edit_uas"]').val(res.uas);
                $('[name="edit_pengetahuan"]').val(res.pengetahuan);
                $('[name="edit_keterampilan"]').val(res.keterampilan);
                $('[name="edit_kkm"]').val(res.kkm);
                $('[name="edit_semester"]').val(res.nama_semester);
                $('#ModalNilaiedit').modal('show');

            },
            error: function ()
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_nilai(id)
    {
       $.ajax({
        url : "<?php echo site_url('Guru/Nilaisiswa/get_by_id')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(res)
        {
         $('[name="delete_id"]').val(res.id);
         $('#ModalHapus').modal('show');
     },
     error: function (jqXHR, textStatus, errorThrocwn)
     {
         alert('Error get data from ajax');
     }
 });
   }


   $("#formEdit").submit(function(e) {
    var url,data,json,post,button,form;
    url = '<?php echo base_url();?>Guru/Nilaisiswa/edit_json';
    data = $(this).serialize();
    form = $("#formEdit input");
    json = 'json';
    post = 'post';
    button = $("#btnedit");
    button.removeClass('btn-primary').addClass('btn-default');
    form.prop('disabled', true);
    event.preventDefault();
    setTimeout(function() {
      $.ajax({
        method   : post,
        url      : url,
        dataType : json,
        data     : data,
        success: function(res){
            if (res.status == 200) {
                notif('success', 'Berhasil Mengubah Niali Siswa');
                $('#ModalNilaiedit').modal('hide');
                reload_table();
                form.prop('disabled', false);
                button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                button.attr('disabled',false);

            }else if(! $.isEmptyObject(res.errors)){

                notif('info', res.errors);
                $('#ModalNilaiedit').modal('hide');
                reload_table();
                form.prop('disabled', false);
                button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                button.attr('disabled',false);
            }
        },
        error: function(){
            notif('info', 'Mengubah Nilai Siswa Error');
            $('#ModalNilaiedit').modal('hide');
            reload_table();
            console.log('500 | Ubah Nilai Siswa Error');
            button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
            button.attr('disabled',false);
            form.prop('disabled', false);
        }
    })
  }, 210);
});

   $("#tambah").click(function(event) {
     setTimeout(function(){
      ClearFormData("#formTambah");
		    $('#ModalNilai').modal('show'); // show bootstrap modal
      }, 550);
 });
   $("#formTambah").submit(function(e) {
     var url,data,json,post,button,form;
     url = '<?php echo base_url();?>Guru/Nilaisiswa/add_json';
     data = $(this).serialize();
     form = $("#formTambah input");
     json = 'json';
     post = 'post';
     button = $("#btntambah");
     button.removeClass('btn-primary').addClass('btn-default');
     form.prop('disabled', true);
     event.preventDefault();
     setTimeout(function() {
      $.ajax({
        method   : post,
        url      : url,
        dataType : json,
        data     : data,
   /*     headers  : 'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content'),*/
        
        success: function(res){
            if (res.status == 200) {
                notif('success', 'Berhasil Menambahkan Nilai Siswa');
                $('#ModalNilai').modal('hide');
                reload_table();
                form.prop('disabled', false);
                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);

            }else if(! $.isEmptyObject(res.errors)){

                notif('info', res.errors);
                $('#ModalNilai').modal('hide');
                reload_table();
                form.prop('disabled', false);
                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
            }
        },
        error: function(){
            notif('info', 'Menambah Nilai Siswa Error');
            $('#ModalNilai').modal('hide');
            reload_table();
            console.log('500 | Tambah Nilai Siswa Error');
            button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
            form.prop('disabled', false);
        }
    })
  }, 210);
 });


   $("#btndelete").click(function(e) {
    var url,data,json,post,button;
    var id = $('#nilai_delete_id').val();
    url = '<?php echo base_url();?>Guru/Nilaisiswa/delete_json';
    json = 'json';
    post = 'post';
    button = $("#btndelete");
    button.removeClass('btn-danger').addClass('btn-default');
    setTimeout(function() {
      $.ajax({
        method   : post,
        url      : url,
        dataType : json,
        data     : {id:id},
        success: function(res){
            if (res.status == 200) {
                notif('success', 'Berhasil Menghapus Nilai Siswa');
                $('#ModalHapus').modal('hide');
                reload_table();
                button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);

            }else if(res.status == 400){

                notif('error', 'Gagal Menghapus Nilai Siswa');
                $('#ModalHapus').modal('hide');
                reload_table();
                button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);
            }
        },
        error: function(){
            notif('info', 'Menghapus Nilai Siswa Error');
            $('#ModalHapus').modal('hide');
            reload_table();
            console.log('500 | Tambah Nilai Siswa Error');
            button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false)
        }
    })
  }, 210);
});


   function reload_table()
   {
      table.ajax.reload(null,false); //reload datatable ajax
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nama').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>Guru/Nilaisiswa/get_subnama",
                method : "GET",
                data : {id:id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_murid+'>'+data[i].nama+'</option>';
                    }
                    $('.namasiswa').html(html);

                }
            });
        });
    });
</script>
