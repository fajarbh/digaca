<div class="content-main" id="content-main">
                <div class="padding">
                    <div class="px-lg-5">
                        <h3 class="text-md mb-4 _400">Dashboard Overview</h3>
                        <div class="row">
                            <div class="col-6 col-lg-3">
                                <div class="box p-3 primary theme">
                                    <div class="d-flex"><span class="text-muted">Total revenue</span></div>
                                    <div class="py-3 text-center text-lg text-white">$35,340</div>
                                    <div class="d-flex"><span class="flex text-muted">Income</span> <span><i class="fa fa-caret-up text-white"></i> 12%</span></div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3">
                                <div class="box p-3 success">
                                    <div class="d-flex"><span class="text-muted">Order status</span></div>
                                    <div class="py-3 text-center text-lg text-white">2,560</div>
                                    <div class="d-flex"><span class="flex text-muted">New order</span> <span><i class="fa fa-caret-down"></i> 5%</span></div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3">
                                <div class="box p-3 warning">
                                    <div class="d-flex"><span class="text-muted">Income status</span></div>
                                    <div class="py-3 text-center text-lg text-white">$6,190</div>
                                    <div class="d-flex"><span class="flex text-muted">New income</span> <span><i class="fa fa-caret-up"></i> 1%</span></div>
                                </div>
                            </div>
                            <div class="col-6 col-lg-3">
                                <div class="box p-3 danger">
                                    <div class="d-flex"><span class="text-muted">Customer status</span></div>
                                    <div class="py-3 text-center text-lg text-white">3,320</div>
                                    <div class="d-flex"><span class="flex text-muted">New users</span> <span><i class="fa fa-caret-down"></i> 11%</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-header"><a href="#" class="float-right text-muted text-sm">See detail</a>
                                        <h3>World sellings</h3></div>
                                    <div class="box-body">
                                        <div>
                                            <div id="jqvmap-world" data-plugin="vectorMap" style="height:180px"></div>
                                        </div>
                                        <div class="p-1"><small><span class="float-right text-muted">45%</span> USA</small>
                                            <div class="progress my-1" style="height:6px">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated primary" style="width: 45%"></div>
                                            </div>
                                        </div>
                                        <div class="p-1"><small><span class="float-right text-muted">25%</span> Germany</small>
                                            <div class="progress my-1" style="height:6px">
                                                <div class="progress-bar warning" style="width: 25%"></div>
                                            </div>
                                        </div>
                                        <div class="p-1"><small><span class="float-right text-muted">15%</span> France</small>
                                            <div class="progress my-1" style="height:6px">
                                                <div class="progress-bar danger" style="width: 15%"></div>
                                            </div>
                                        </div>
                                        <div class="p-1"><small><span class="float-right text-muted">5%</span> Other</small>
                                            <div class="progress my-1" style="height:6px">
                                                <div class="progress-bar grey lt" style="width: 5%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer pt-1"><small class="text-muted">Tip: All data caculated in real time</small></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-header"><a href="#" class="float-right text-muted text-sm">See detail</a>
                                        <h3>Summary</h3></div>
                                    <div class="box-body">
                                        <canvas id="chart-doughnut-3" data-plugin="chart" height="102"></canvas>
                                    </div>
                                    <div class="list">
                                        <div class="list-item">
                                            <div><i class="badge badge-xs badge-o b-success"></i> <span class="mx-2">20.2%</span></div>
                                            <div class="list-body"><span class="text-muted">Search engine</span></div><span><i class="fa fa-caret-up text-success"></i></span></div>
                                        <div class="list-item">
                                            <div><i class="badge badge-xs badge-o b-warning"></i> <span class="mx-2">15.2%</span></div>
                                            <div class="list-body"><span class="text-muted">Social media</span></div><span><i class="fa fa-caret-up text-warning"></i></span></div>
                                        <div class="list-item">
                                            <div><i class="badge badge-xs badge-o b-danger"></i> <span class="mx-2">35.5%</span></div>
                                            <div class="list-body"><span class="text-muted">Direct</span></div><span><i class="fa fa-caret-down text-danger"></i></span></div>
                                        <div class="list-item">
                                            <div><i class="badge badge-xs badge-o b-dark"></i> <span class="mx-2">5.5%</span></div>
                                            <div class="list-body"><span class="text-muted">Other</span></div>
                                        </div>
                                    </div>
                                    <div class="box-footer"><small class="text-muted">Tip: All data caculated in real time</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-header">
                                <h3>Market overview</h3><small>Sale information on advertising, exhibitions, market research, online media, customer desires, PR and much more</small></div>
                            <div class="box-body">
                                <canvas id="chart-line" data-plugin="chart" height="60"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3>Messages</h3></div>
                                    <div class="list inset">
                                        <div class="list-item" data-id="item-10"><span class="w-40 avatar circle blue"><img src="../assets/images/a10.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Postiljonen</a>
                                                <div class="item-except text-sm text-muted h-1x">Looking for some client-work</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div><span class="item-date text-xs text-muted">08:00</span></div>
                                        </div>
                                        <div class="list-item" data-id="item-3"><span class="w-40 avatar circle green"><img src="../assets/images/a3.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Jeremy Scott</a>
                                                <div class="item-except text-sm text-muted h-1x">Submit a support ticket</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div><span class="item-date text-xs text-muted">09:05</span></div>
                                        </div>
                                        <div class="list-item" data-id="item-11"><span class="w-40 avatar circle blue">TN</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Tiger Nixon</a>
                                                <div class="item-except text-sm text-muted h-1x">Looking for some client-work</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div><span class="item-date text-xs text-muted">16:00</span></div>
                                        </div>
                                        <div class="list-item" data-id="item-1"><span class="w-40 avatar circle grey"><img src="../assets/images/a1.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Summerella</a>
                                                <div class="item-except text-sm text-muted h-1x">Send you a message</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div><span class="item-date text-xs text-muted">July 21</span></div>
                                        </div>
                                        <div class="list-item" data-id="item-15"><span class="w-40 avatar circle indigo">GW</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Garrett Winters</a>
                                                <div class="item-except text-sm text-muted h-1x">Looking for some client-work</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div><span class="item-date text-xs text-muted">13:00</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3>Members</h3></div>
                                    <div class="list inset">
                                        <div class="list-item" data-id="item-15"><span class="w-40 avatar circle indigo"><i class="on b-white avatar-right"></i> GW</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Garrett Winters</a>
                                                <div class="item-except text-sm text-muted h-1x">garrett-winters@gmail.com</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-7"><span class="w-40 avatar circle indigo"><i class="on b-white avatar-right"></i> FH</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Fifth Harmony</a>
                                                <div class="item-except text-sm text-muted h-1x">fifth-harmony@gmail.com</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-11"><span class="w-40 avatar circle blue"><i class="on b-white avatar-right"></i> TN</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Tiger Nixon</a>
                                                <div class="item-except text-sm text-muted h-1x">tiger-nixon@gmail.com</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-14"><span class="w-40 avatar circle brown"><i class="off b-white avatar-right"></i> BW</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Brielle Williamson</a>
                                                <div class="item-except text-sm text-muted h-1x">brielle-williamson@gmail.com</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-9"><span class="w-40 avatar circle cyan"><i class="on b-white avatar-right"></i> PN</span>
                                            <div class="list-body"><a href="#" class="item-title _500">Pablo Nouvelle</a>
                                                <div class="item-except text-sm text-muted h-1x">pablo-nouvelle@gmail.com</div>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="padding box">
                                    <h6 class="mb-3">Activity</h6>
                                    <div class="streamline streamline-dotted">
                                        <div class="sl-item b-">
                                            <div class="sl-left"><img src="../assets/images/a7.jpg" class="circle" alt="."></div>
                                            <div class="sl-content"><span class="sl-date text-muted">05:35</span>
                                                <div><a href="#" class="text-primary">Fifth Harmony</a> Bug fixes</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-">
                                            <div class="sl-left"><img src="../assets/images/a3.jpg" class="circle" alt="."></div>
                                            <div class="sl-content"><span class="sl-date text-muted">09:05</span>
                                                <div><a href="#" class="text-primary">Jeremy Scott</a> Assign you a task</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-">
                                            <div class="sl-left"><img src="../assets/images/a4.jpg" class="circle" alt="."></div>
                                            <div class="sl-content"><span class="sl-date text-muted">12:05</span>
                                                <div><a href="#" class="text-primary">Judith Garcia</a> Follow up to close deal</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-">
                                            <div class="sl-left"><img src="../assets/images/a6.jpg" class="circle" alt="."></div>
                                            <div class="sl-content"><span class="sl-date text-muted">13:23</span>
                                                <div><a href="#" class="text-primary">Rita Ora</a> Sent you an email</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-">
                                            <div class="sl-left"><img src="../assets/images/a9.jpg" class="circle" alt="."></div>
                                            <div class="sl-content"><span class="sl-date text-muted">15:00</span>
                                                <div><a href="#" class="text-primary">Pablo Nouvelle</a> Added API call to update track element</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="padding box"><span class="badge success float-right">5</span>
                                    <h6 class="mb-3">Tasks</h6>
                                    <div class="streamline streamline-dotted">
                                        <div class="sl-item b-success">
                                            <div class="sl-content"><span class="sl-date text-muted">14:00</span>
                                                <div><a href="#" class="text-primary">RYD</a> Add inline SVG icon</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-success">
                                            <div class="sl-content"><span class="sl-date text-muted">08:00</span>
                                                <div><a href="#" class="text-primary">Postiljonen</a> Add Google Cast support</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-success">
                                            <div class="sl-content"><span class="sl-date text-muted">July 21</span>
                                                <div><a href="#" class="text-primary">Summerella</a> Submit a support ticket</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-success">
                                            <div class="sl-content"><span class="sl-date text-muted">13:00</span>
                                                <div><a href="#" class="text-primary">Garrett Winters</a> Followed by Jessic</div>
                                            </div>
                                        </div>
                                        <div class="sl-item b-">
                                            <div class="sl-content"><span class="sl-date text-muted">08:00</span>
                                                <div><a href="#" class="text-primary">Brielle Williamson</a> User experience improvements</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="padding box">
                                    <h6>Friends</h6>
                                    <div class="list inset">
                                        <div class="list-item" data-id="item-6"><span class="w-24 avatar circle brown"><img src="../assets/images/a6.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Rita Ora</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-4"><span class="w-24 avatar circle pink"><img src="../assets/images/a4.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Judith Garcia</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-3"><span class="w-24 avatar circle green"><img src="../assets/images/a3.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Jeremy Scott</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-7"><span class="w-24 avatar circle indigo"><img src="../assets/images/a7.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Fifth Harmony</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-2"><span class="w-24 avatar circle light-blue"><img src="../assets/images/a2.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Kygo</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-item" data-id="item-5"><span class="w-24 avatar circle blue-grey"><img src="../assets/images/a5.jpg" alt="."></span>
                                            <div class="list-body"><a href="#" class="item-title _500">Radionomy</a>
                                                <div class="item-tag tag hide"></div>
                                            </div>
                                            <div>
                                                <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right text-color" role="menu"><a class="dropdown-item"><i class="fa fa-tag"></i> Action </a><a class="dropdown-item"><i class="fa fa-pencil"></i> Another action </a><a class="dropdown-item"><i class="fa fa-reply"></i> Something else here</a>
                                                        <div class="dropdown-divider"></div><a class="dropdown-item"><i class="fa fa-ellipsis-h"></i> Separated link</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            