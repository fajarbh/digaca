  	<script src="<?php echo config_item('js');?>app.js"></script>       
    <script src="<?php echo config_item('js');?>popper.js"></script>
    <!-- bootstrap -->
    <script src="<?php echo config_item('asset');?>libs/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo config_item('asset');?>libs/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="<?php echo config_item('js');?>bootstrap-datepicker.js"></script>	
	<script>
		
		$('#datepicker').datepicker({
	    format: 'mm/dd/yyyy',
        autoclose: true,
    	todayHighlight: true
		});

	</script>
</body>
</html>