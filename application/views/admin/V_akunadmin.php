<script>

			function add_person()
		{
		    $('#validation')[0].reset(); // reset form on modals
		    $('#ModalKelas').modal('show'); // show bootstrap modal
		}

		// function edit_kelas(id)
		// {
		// $('#form')[0].reset(); // reset form on modals
		// //Ajax Load data from ajax
		// $.ajax({
		// 	url : "<?php echo site_url('Admin/akunadmin/get_by_id')?>/" + id,
		// 	type: "GET",
		// 	dataType: "JSON",
		// 	success: function(data)
		// 	{
		// 		$('[name="edit_id"]').val(data.id);
		// 		$('[name="edit_nama"]').val(data.nama);
		// 		$('#ModalEditKelas').modal('show');  
		// 	},
		// 	error: function (jqXHR, textStatus, errorThrown)
		// 	{
		// 	}
		// });
		// } 

		function delete_akun(id)
		{
			$.ajax({
				url : "<?php echo site_url('Admin/akunadmin/get_by_id')?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
					$('[name="delete_id"]').val(data.id);
					$('#ModalHapusAkun').modal('show');  
				},
				error: function (jqXHR, textStatus, errorThrown)
				{

				}
			});
		} 
</script>
		<!-- start page content -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="page-bar">
					<div class="page-title-breadcrumb">
						<div class=" pull-left">
							<div class="page-title">Data Admin</div>
						</div>
						<ol class="breadcrumb page-breadcrumb pull-right">
							<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
								href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
							</li>
							<li class="active">Akun Admin</li>
						</ol>
					</div>
				</div>

				<!-- start new student list -->

				<div class="row">
					<div class="col-md-10"></div>

					<div class="col-md-2">
						<button data-target="#ModalAkunadmin" data-toggle="modal"class="btn btn-primary">Tambah Data</button>
					</div>

					<div class="col-md-12 col-sm-12">
						<div class="card card-topline-aqua">
							<div class="card-head">
								<header>Tabel Akun Admin</header>
							</div>
							<div class="card-body ">
								<div class="table-wrap">
									<div class="table-responsive-sm">
										<table width="100%" class="display admin" class="display">
											<thead>
												<tr>
													<th>No</th>
													<th>Nama</th>
													<th>Email</th>
													<th>Action</th>	
												</tr>
											</thead>
											<tbody>
												<tr>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

				<!-- Modal tambah Kelas -->
		<div class="modal fade" id="ModalAkunadmin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="validation">  
							<h4 class="text-center" style="padding-bottom: 15px">Tambah Admin</h4>
							<div class="form-group col-md-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="mdl-textfield__label">Nama</label>
									<input class="mdl-textfield__input" type="text"  name="nama" required minlength="4" maxlength="20">
								</div>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="mdl-textfield__label">Email</label>
									<input class="mdl-textfield__input" type="email"  name="email" required minlength="5" maxlength="50">
								</div>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="mdl-textfield__label">Password</label>
									<input class="mdl-textfield__input" type="password"  name="password" required minlength="6" maxlength="20">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-8"></div>
								<div class="col-md-2">
									<button class="btn btn-primary btn-sm" id="btnSave" style="color: #009688;" type="submit">SIMPAN</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--   End Modal -->

		<!-- Hapus akunadmin -->
		<div id="ModalHapusAkun" class="modal fade">
		  <div class="modal-dialog modal-confirm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <div class="icon-box">
		          <i class="material-icons">&#xE5CD;</i>
		        </div>
		      </div>
		      <div class="modal-body">
		        <input type="hidden" name="delete_id">
		        <p>Apakah anda yakin data ingin di hapus?</p>
		      </div>
		      <div class="model-footer">
		      <div class="row">
		    	<div class="col-md-2"></div>
		    	<div class="col-md-8">
		        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
		        <button type="button" class="btn btn-danger" id="hapus">Delete</button>
		        </div>
		      	<div class="col-md-2"></div>
		      </div>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- End Hapus -->


<script src="<?php echo config_item('js');?>jquery.min.js"></script>
<script>
	$(document).ready(function() {
	//datatables
	table = $('.admin').DataTable({ 

        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/akunadmin/akun_list_json')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        });
		});

	 $("#validation").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/register';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method   : post,
    url      : url,
    dataType : json,
    data     : data,
    success : function(data){
    {
    	$('#validation').each(function(){
		this.reset();});
        $('#ModalAkunadmin').modal('hide');
        reload_table();
    }
    },
       error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
  });
  return false;
});
	 	  $('#hapus').on('click',function(){
	  	var delete_id=$('[name="delete_id"]').val();
	  	$.ajax({
	  		type : "POST",
	  		url  : "<?php echo base_url('Admin/akunadmin/delete_json')?>",
	  		dataType : "JSON",
	  		data :{delete_id:delete_id},
	  		success: function(data){
	
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusAkun').modal('hide');                     
	  		},
	  		error: function (jqXHR,textStatus,data,errorThrown)
	  		{
	  			
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusKelas').modal('hide');                     
	  		}

	  	});
	  	return false;
	  });


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
</script>