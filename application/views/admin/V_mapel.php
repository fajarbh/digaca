<script>

			function add_person()
		{
		    $('#validation')[0].reset(); // reset form on modals
		    $('#ModalKelas').modal('show'); // show bootstrap modal
		}

		function edit_mapel(id)
		{
		$('#form')[0].reset(); // reset form on modals
		//Ajax Load data from ajax
		$.ajax({
			url : "<?php echo site_url('Admin/mapel/get_by_id')?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
				$('[name="edit_id"]').val(data.id);
				$('[name="edit_kode_mapel"]').val(data.kode_mapel);
				$('[name="edit_nama_mapel"]').val(data.nama_mapel);
				$('#ModalEditMapel').modal('show');  
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		});
		} 

		function delete_mapel(id)
		{
			$.ajax({
				url : "<?php echo site_url('Admin/mapel/get_by_id')?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
					$('[name="delete_id"]').val(data.id);
					$('#ModalHapusMapel').modal('show');  
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error get data from ajax');
				}
			});
		} 
</script>

		<!-- start page content -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="page-bar">
					<div class="page-title-breadcrumb">
						<div class=" pull-left">
							<div class="page-title">Data Mapel</div>
						</div>
						<ol class="breadcrumb page-breadcrumb pull-right">
							<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
								href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
							</li>
							<li class="active">Mapel</li>
						</ol>
					</div>
				</div>

				<!-- start new student list -->

				<div class="row">
					<div class="col-md-10"></div>

					<div class="col-md-2">
						<button data-target="#ModalKelas" data-toggle="modal"class="btn btn-primary">Tambah Data</button>
					</div>

					<div class="col-md-12 col-sm-12">
						<div class="card card-topline-aqua">
							<div class="card-head">
								<header>Tabel Mapel</header>
							</div>
							<div class="card-body ">
								<div class="table-wrap">
									<div class="table-responsive-sm">
										<table width="100%" class="display mapel" class="display">
											<thead>
												<tr>
													<th>No</th>
													<th>Kode Mapel</th>
													<th>Nama Mapel</th>
													<th>Action</th>	
												</tr>
											</thead>
											<tbody>
												<tr>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- Modal tambah Mapel -->
		<div class="modal fade" id="ModalKelas" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="validation">  
							<h4 class="text-center" style="padding-bottom: 15px">Tambah Mapel</h4>
							<div class="form-group col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="add_kode_mapel" required>
									<label class="mdl-textfield__label" for="text5">Kode Mapel </label>
									<span class="mdl-textfield__error">Number required!</span>		
								</div>
							</div>
							<div class="form-group col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="mdl-textfield__label">Nama Mapel</label>
									<input class="mdl-textfield__input" type="text" required="" name="add_nama_mapel" required>
							        <span class="help-block"></span>		
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-8"></div>
								<div class="col-md-2">
									<button class="btn btn-primary btn-sm" id="btnSave" style="color: #009688;" type="submit">SIMPAN</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--   End Modal -->

		<!-- Modal Edit Mapel -->
		<div class="modal fade" id="ModalEditMapel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="form">  
							<h4 class="text-center" style="padding-bottom: 15px">Edit Mapel</h4>
							<input type="hidden" name="edit_id">
							<div class="form-group col-md-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="edit_kode_mapel" placeholder="Kode Mapel" required>
								<span class="mdl-textfield__error">Number required!</span>		
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<input class="mdl-textfield__input" placeholder="Nama Mapel" type="text" id="text5" name="edit_nama_mapel" required>
								<span class="mdl-textfield__error"></span>		
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-7"></div>
								<div class="col-md-3">
									<button class="btn btn-primary btn-sm" style="color: #009688;" type="submit">SIMPAN</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Edit Mapel  -->

		<!-- Hapus Mapel -->
		<div id="ModalHapusMapel" class="modal fade">
		  <div class="modal-dialog modal-confirm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <div class="icon-box">
		          <i class="material-icons">&#xE5CD;</i>
		        </div>
		      </div>
		      <div class="modal-body">
		        <input type="hidden" name="delete_id">
		        <p>Apakah anda yakin data ingin di hapus?</p>
		      </div>
		      <div class="model-footer">
		      <div class="row">
		    	<div class="col-md-2"></div>
		    	<div class="col-md-8">
		        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
		        <button type="button" class="btn btn-danger" id="hapus">Delete</button>
		        </div>
		      	<div class="col-md-2"></div>
		      </div>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- End Hapus -->


<script src="<?php echo config_item('js');?>jquery.min.js"></script>
<script>

	var table;

$(document).ready(function() {

    //datatables
    table = $('.mapel').DataTable({ 

        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/mapel/mapel_list_json')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        });

    	$("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});

  $("#validation").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/mapel/add_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method   : post,
    url      : url,
    dataType : json,
    data     : data,
    success : function(data){
    if(data.status) //if success close modal and reload ajax table
    {
    	$('#validation').each(function(){
		this.reset();});
        $('#ModalKelas').modal('hide');
        reload_table();
    }
    },
       error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
  });
  return false;
});



  $("#form").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/mapel/edit_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method : post,
    url    : url,
    dataType : json,
    data : data,
    success : function(data){
    $('[name="edit_id"]').val("");
    $('[name="edit_kode_mapel"]').val("");
	$('[name="edit_nama_mapel"]').val("");
    $('#ModalEditMapel').modal('hide');  
    alert("Berhasil");
    reload_table();    
    },
    error: function (jqXHR,textStatus,data,errorThrown)
  {
  	$('[name="edit_id"]').val("");
    $('[name="edit_kode_mapel"]').val("");
	$('[name="edit_nama_mapel"]').val("");
    $('#ModalEditMapel').modal('hide');  
    alert("Gagal");
    reload_table();    
  }
  });
  return false;
});


	  $('#hapus').on('click',function(){
	  	var delete_id=$('[name="delete_id"]').val();
	  	$.ajax({
	  		type : "POST",
	  		url  : "<?php echo base_url('Admin/mapel/delete_json')?>",
	  		dataType : "JSON",
	  		data :{delete_id:delete_id},
	  		success: function(data){
	  			alert("Data Berhasil dihapus");
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusMapel').modal('hide');                     
	  		},
	  		error: function (jqXHR,textStatus,data,errorThrown)
	  		{
	  			alert("Data Gagal dihapus");
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusMapel').modal('hide');                     
	  		}

	  	});
	  	return false;
	  });




  function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

		</script>