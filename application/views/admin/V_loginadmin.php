<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Authentication forms">
    <meta name="author" content="Arasari Studio">

    <title>Digaca</title>
    <link href="<?php echo config_item('css');?>swal.css" rel="stylesheet" type="text/css" /> 

    <link href="<?php echo config_item('css');?>bootstrap.min.css" rel="stylesheet" type="text/css" />   
    <link href="<?php echo config_item('css');?>common.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&amp;display=swap" rel="stylesheet">
    <link href="<?php echo config_item('css');?>theme-07.css" rel="stylesheet">

</head>

<body>
  
    <div class="forny-container">
        
<div class="forny-inner">
    <div class="forny-two-pane">
        <div>
            <div class="forny-form">
                <div class="mb-8 forny-logo">
                    <img src="<?php echo config_item('img');?>8989.png" width="150" height="102">
                </div>

                <div id="alert">
                       <?= $this->session->flashdata('message');?>
                        </div>

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active bg-transparent text-white" href="#login" data-toggle="tab" role="tab">
                            <span>Login</span>
                        </a>
                    </li>
                    <li class="nav-item">
                                <a class="nav-link bg-transparent text-white" href="<?php echo site_url('Siswa/login') ?>" >
                                    <span>Siswa</span>
                                </a>
                            </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active text-white" role="tabpanel" id="login">
            <?php echo form_open('Admin/proseslogin',array('class' => 'loginForm', 'role' => 'login'));?>    
        <div class="input-group" style="
    border:0px ;
    border-bottom: 2px solid #cccccc;
    border-radius: 0px;
    " >
            <div class="input-group-prepend ">
                <span class="input-group-text fas fa-user text-white">
                </span>
            </div>     
      <input type="text" class="form-control  text-white" name="username" minlength="4" maxlength="20" autocomplete="off" autosave="false" required placeholder="Username">
        </div>
    <br>                    
        <div class="input-group"  style="
    border:0px ;
    border-bottom: 2px solid #cccccc;
    border-radius: 0px;
    " >
            <div class="input-group-prepend ">
                <span class="input-group-text fas fa-lock text-white ">
                </span>
            </div>
            
    <input type="password" class="form-control password text-white" name="password" maxlength="20" placeholder="Password" required  minlength="5">

            <div class="input-group-append cursor-pointer">
                <span class="input-group-text">
                </span>
            </div>
        </div>


                            <div class="row mt-10">
                                <div class="col-12">
                                <button type="submit" value="login" class="btn  btn-block btn-warning text-dark" name="submit" >Login</button>
                                </div>
                                
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div></div>
    </div>
</div>
</div>

    <!-- Javascript Files -->

    <script src="<?php echo config_item('js');?>jquery.min.js"></script>

    <script src="<?php echo config_item('js');?>bootstrap.min.js"></script>

    <script src="<?php echo config_item('js')?>swal.min.js"></script>

       <script type="text/javascript">
$(function(){
    var title = '<?= $this->session->flashdata("title") ?>';
    var text = '<?= $this->session->flashdata("text") ?>';
    var type = '<?= $this->session->flashdata("type") ?>';
    if (title) {
        swal({
            title: title,
            text: text,
            type: type,
            button: true,
        });
    };
});

$('#alert').delay('slow').slideDown('slow').delay(4100).slideUp(600);
</script>

  </body>   
</html>