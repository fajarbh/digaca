<div class="content-main" id="content-main">
    <div class="padding">
        <button type="button" class="btn text-white btn-sm" style="background-color:#105cad;" id="tambah">Tambah Siswa</button>
        <div class="btn-group" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle btn-sm text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Excel
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>">Export</a>
                <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>" data-toggle="modal" data-target="#Modalexcel">Import</a>
            </div>
        </div>
        <div class="table-responsive">
            <br>
            <table id="datasiswa" class="table display nowrap v-middle p-0 m-0 box" data-plugin="dataTable" style="font-size: 14px;width: 100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nis</th>
                        <th>Nama</th>
                        <th width="15%">Jenis Kelamin</th>
                        <th>Kelas</th>
                        <th>Tahun Ajaran</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
</div>
</div>


<!-- Modal tambah Murid -->
<div class="modal fade" id="ModalMurid" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-body">
                <form id="formTambah">
                    <h4 class="text-center text-dark" style="padding-bottom: 15px">Tambah Murid</h4>
                    <div class="row">
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">NIS</label>
                            <input class="form-control" type="text" autocomplete="off" autosave="false" required name="add_nis" minlength="6">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Nama Murid</label>
                            <input class="form-control" type="text" autocomplete="off" autosave="false" required name="add_nama">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Kelas</label>
                            <select name="add_kelas" class="form-control" required="">
                                <option value="">-- PILIH --</option>
                                <?php foreach ($kelas as $data) {
                                    $id_kls=$data->id;
                                    $nm_kls=$data->kelas;
                                    echo "<option value='$nm_kls'>$nm_kls</option>";
                                }
                                ?>
                            </select>

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Tahun Ajaran</label>
                            <select name="add_thn_ajaran" class="form-control">
                                <option value="">-- PILIH --</option>
                                <?php foreach ($thn_ajaran as $data) {
                                    $id_ajrn=$data->id;
                                    $nm_ajrn=$data->tahun_ajaran;
                                    echo "<option value='$nm_ajrn' selected>$nm_ajrn</option>";
                                }
                                ?>
                            </select>

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Jenis Kelamin</label>
                            <select name="add_jk" class="form-control" >
                                <option value="">-- PILIH --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>}
                            </select>

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Tanggal Lahir</label>
                            <input class="form-control " id="datepicker" type="text" name="add_tgl_lhr">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Agama</label>
                            <input class="form-control" type="text" required="" name="add_agama">

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">No HP</label>
                            <input class="form-control" type="text" name="add_nohp" value=""
                            autocomplete="off" autosave="false" required>

                        </div>
                        <!-- ========================================================================================= -->
                        <div class="form-group col-md-6">

                            <label class="form-check-label" style="font-size: 13px;color:grey;">Email</label>
                            <input class="form-control" type="email" name="add_email" value="">

                        </div>
                        <!-- ========================================================================================= -->
                    </div>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-lg-3">
                            <button type="submit"
                            class="btn btn-sm btn-primary text-white" id="btntambah">Tambah</button>
                            <button type="button"
                            class="btn btn-sm btn-default">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--   End Modal -->

<!-- Modal tambah Murid -->
<div class="modal fade" id="Modalexcel" tabindex="-1" role="dialog" aria-labelledby="ModalexcelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalexcelLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=site_url()?>Admin/import" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Lampirkan File</label>
                        <a href="<?php echo base_url("excel/format.xlsx"); ?>" class="btn btn-sm btn-primary text-white pull-right">Download Format</a>
                        <div class="col-sm-10">
                            <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><font color="white">Close</font></button>
                    <input type="submit" class="btn btn-primary text-white" value="Import" name="import"/>
                </div>
            </form>
        </div>
    </div>
</div>
<!--   End Modal -->
