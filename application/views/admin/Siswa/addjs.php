<script>

	    var table;

$(document).ready(function() {
    table = $('#datasiswa').DataTable({ 
        "processing": false, //Feature control the processing indicator.
        ajax: "<?php echo site_url('Admin/murid/murid_list_json')?>",
        stateSave : true,
    });
});

	   $("#tambah").click(function(event) {
		   setTimeout(function(){
		    ClearFormData("#formTambah");
		    $('#ModalMurid').modal('show'); // show bootstrap modal
		    }, 550);
        });

                $("#formTambah").submit(function(e) {
            	var url,data,json,post,button,form;
                url = '<?php echo base_url();?>Admin/murid/add_json';
                data = $(this).serialize();
                form = $("#formTambah input select");
                json = 'json';
                post = 'post';
                button = $("#btntambah");
                button.removeClass('btn-primary').addClass('btn-default');
                form.prop('disabled', true);
                event.preventDefault();
                setTimeout(function() {
                  $.ajax({
                    method   : post,
                    url      : url,
                    dataType : json,
                    data     : data,
                 success: function(res){
                            if (res.status == 200) {
                                notif('success', 'Berhasil Menambahkan Murid');
                                $('#ModalMurid').modal('hide');
                                reload_table();
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);

                            }else if(! $.isEmptyObject(res.errors)){
    
                                notif('error', res.errors);
                                $('#ModalMurid').modal('hide'); 
                                reload_table(); 
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                            }
                        },
                        error: function(){
                            notif('info', 'Menambah Murid Error');
                            $('#ModalMurid').modal('hide');  
                            reload_table();
                            console.log('500 | Tambah Murid Error');
                            button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                            form.prop('disabled', false);
                        }
                    })
                }, 210);
                    });

                                  function reload_table()
                {
                    table.ajax.reload(null,false); //reload datatable ajax 
                }
	   
</script>