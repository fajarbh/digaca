<script>


    var table;

$(document).ready(function() {
    table = $('#datamapel').DataTable({
        "processing": false, //Feature control the processing indicator.
        ajax: "<?php echo site_url('Admin/mapel/mapel_list_json')?>",
        stateSave : true,
        "order": [], //Initial no order.

    });
});



        function edit_mapel(id)
        {
        $('#formEdit')[0].reset(); // reset form on modals
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('Admin/mapel/get_by_id')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(res)
            {

                $('[name="edit_id"]').val(res.id);
                $('[name="edit_kode"]').val(res.kode_mapel);
                $('[name="edit_mapel"]').val(res.nama_mapel);
                $('#modal_mapeledit').modal('show');

            },
            error: function ()
            {
                alert('Error get data from ajax');
            }
        });
        }

        function delete_mapel(id)
		{
			$.ajax({
				url : "<?php echo site_url('Admin/mapel/get_by_id')?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function(res)
				{
					$('[name="delete_id"]').val(res.id);
					$('#ModalHapus').modal('show');
				},
				error: function (jqXHR, textStatus, errorThrocwn)
				{
					alert('Error get data from ajax');
				}
			});
		}


        $("#formEdit").submit(function(e) {
                var url,data,json,post,button,form;
                url = '<?php echo base_url();?>Admin/mapel/edit_json';
                data = $(this).serialize();
                form = $("#formEdit input");
                json = 'json';
                post = 'post';
                button = $("#btnedit");
                button.removeClass('btn-primary').addClass('btn-default');
                form.prop('disabled', true);
                event.preventDefault();
                setTimeout(function() {
                  $.ajax({
                    method   : post,
                    url      : url,
                    dataType : json,
                    data     : data,
                 success: function(res){
                            if (res.status == 200) {
                                notif('success', 'Berhasil Mengubah Mapel');
                                $('#modal_mapeledit').modal('hide');
                                reload_table();
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                                button.attr('disabled',false);

                            }else if(! $.isEmptyObject(res.errors)){

                                notif('info', res.errors);
                                $('#modal_mapeledit').modal('hide');
                                reload_table();
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                                button.attr('disabled',false);
                            }
                        },
                        error: function(){
                            notif('info', 'Mengubah Mapel Error');
                            $('#modal_mapeledit').modal('hide');
                            reload_table();
                            console.log('500 | Ubah Mapel Error');
                            button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                            button.attr('disabled',false);
                            form.prop('disabled', false);
                        }
                    })
                }, 210);
                    });

        $("#tambah").click(function(event) {
		   setTimeout(function(){
		    ClearFormData("#formTambah");
		    $('#modal_mapel').modal('show'); // show bootstrap modal
		    }, 550);
        });
                $("#formTambah").submit(function(e) {
            	var url,data,json,post,button,form;
                url = '<?php echo base_url();?>Admin/mapel/add_json';
                data = $(this).serialize();
                form = $("#formTambah input");
                json = 'json';
                post = 'post';
                button = $("#btntambah");
                button.removeClass('btn-primary').addClass('btn-default');
                form.prop('disabled', true);
                event.preventDefault();
                setTimeout(function() {
                  $.ajax({
                    method   : post,
                    url      : url,
                    dataType : json,
                    data     : data,
                 success: function(res){
                            if (res.status == 200) {
                                notif('success', 'Berhasil Menambahkan Mapel');
                                $('#modal_mapel').modal('hide');
                                reload_table();
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);

                            }else if(! $.isEmptyObject(res.errors)){

                                notif('info', res.errors);
                                $('#modal_mapel').modal('hide');
                                reload_table();
                                form.prop('disabled', false);
                                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                            }
                        },
                        error: function(){
                            notif('info', 'Menambah Mapel Error');
                            $('#modal_mapel').modal('hide');
                            reload_table();
                            console.log('500 | Tambah Mapel Error');
                            button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                            form.prop('disabled', false);
                        }
                    })
                }, 210);
                    });


                $("#btndelete").click(function(e) {
                var url,data,json,post,button;
                var id = $('#mapel_delete_id').val();
                url = '<?php echo base_url();?>Admin/mapel/delete_json';
                json = 'json';
                post = 'post';
                button = $("#btndelete");
                button.removeClass('btn-danger').addClass('btn-default');
                setTimeout(function() {
                  $.ajax({
                    method   : post,
                    url      : url,
                    dataType : json,
                    data     : {id:id},
                 success: function(res){
                            if (res.status == 200) {
                                notif('success', 'Berhasil Menghapus Mapel');
                                $('#ModalHapus').modal('hide');
                                reload_table();
                                button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);

                            }else if(res.status == 400){

                                notif('error', 'Gagal Menghapus Mapel');
                                $('#ModalHapus').modal('hide');
                                reload_table();
                                button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);
                            }
                        },
                        error: function(){
                            notif('info', 'Menghapus Mapel Error');
                            $('#ModalHapus').modal('hide');
                            reload_table();
                            console.log('500 | Tambah Mapel Error');
                            button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false)
                        }
                    })
                }, 210);
                    });


                  function reload_table()
                {
                    table.ajax.reload(null,false); //reload datatable ajax
                }


</script>
