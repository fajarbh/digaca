<div class="content-main" id="content-main">
  <div class="padding">
    <!--<button type="" onclick="reload_tabel()">Reload</button> -->
    <button type="button" class="btn text-white btn-sm" style="background-color:#105cad;" id="tambah">Tambah Mapel</button>
    <div class="btn-group" role="group">
      <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle btn-sm text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Excel
      </button>
      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>">Export</a>
        <a class="dropdown-item" href="<?php echo site_url('Admin/export'); ?>">Import</a>
      </div>
    </div>
    
    <div class="table-responsive">
      <br>
      <table id="datamapel" class="table v-middle p-0 m-0 box" data-plugin="dataTable" style="font-size: 14px">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Mapel</th>
            <th>Nama Mapel</th>
            <th>Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
</div>
</div>


<!-- Modal Mapel -->
<div class="modal fade" id="modal_mapel" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm  modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <form id="formTambah">
          <h4 class="text-center text-dark" style="padding-bottom: 15px;">Tambah Mapel</h4>
          <div class="form-group col-md-12">
            <div class="border-bottom">
              <label for="">Kode Mapel</label>
              <input class="form-control" type="text" name="add_kode" placeholder="Kode Mapel" autocomplete="off" autosave="false" required="on" onkeypress="return hanyaAngka(event)">
              <span></span>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="border-bottom">
              <label for="">Nama Mapel</label>
              <input class="form-control" type="text" name="add_mapel" placeholder="Nama Mapel" autocomplete="off" autosave="false" required="on">
              <span></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-3">
              <button type="submit" style="color:#105cad;" class="btn btn-primary btn-sm text-white" id="btntambah">Tambah</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--   End Modal -->

<!-- Modal Mapel Edit -->
<div class="modal fade" id="modal_mapeledit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm  modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <form id="formEdit">
          <h4 class="text-center text-dark" style="padding-bottom: 15px;">Edit Kelas</h4>
          <div class="form-group col-md-12">
            <div class="border-bottom">
              <label for="">Kode Mapel</label>
              <input type="hidden" name="edit_id">
              <input class="form-control" type="text" name="edit_kode" placeholder="Kode Mapel" autocomplete="off" autosave="false" required="on" onkeypress="return hanyaAngka(event)">
              <span></span>
              </div>
            </div>
          <div class="form-group col-md-12">
            <div class="border-bottom">
              <label for="">Nama Mapel</label>
              <input class="form-control" type="text" name="edit_mapel" placeholder="Nama Mapel" autocomplete="off" autosave="false" required="on">
              <span></span>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-3">
              <button type="submit" style="color:#105cad;" class="btn btn-primary btn-sm text-white" id="btnedit">Edit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--   End Modal -->

<!-- Hapus Kelas -->
<div id="ModalHapus" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE5CD;</i>
        </div>
      </div>
      <div class="modal-body">
        <input type="hidden" name="delete_id" id="mapel_delete_id">
        <p>Apakah anda yakin data ingin di hapus?</p>
      </div>
      <div class="model-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" id="btndelete">Delete</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Hapus -->

<!-- Hanya Angka -->
<script>
  function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

      return false;
    return true;
  }
</script>
