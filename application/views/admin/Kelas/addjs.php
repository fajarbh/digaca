<script>


    var table;

    $(document).ready(function() {
    table = $('#datakelas').DataTable({ 
    "processing": false, //Feature control the processing indicator.
    ajax: "<?php echo site_url('Admin/kelas/kelas_list_json')?>",   
    stateSave : true,
    "order": [], //Initial no order.
    });
        });



    function edit_kelas(id)
    {
$('#formEdit')[0].reset(); // reset form on modals
//Ajax Load data from ajax
$.ajax({
    url : "<?php echo site_url('Admin/kelas/get_by_id')?>/" + id,
    type: "GET",
    dataType: "JSON",
    success: function(res)
    {

        $('[name="edit_id"]').val(res.id);
        $('[name="edit_kelas"]').val(res.kelas);
        $('#modal_kelasedit').modal('show');  

    },
    error: function ()
    {
        alert('Error get data from ajax');
    }
});
}

function delete_kelas(id)
{
    $.ajax({
        url : "<?php echo site_url('Admin/kelas/get_by_id')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(res)
        {
            $('[name="delete_id"]').val(res.id);
            $('#ModalHapus').modal('show');  
        },
        error: function (jqXHR, textStatus, errorThrocwn)
        {
            alert('Error get data from ajax');
        }
    });
} 


$("#formEdit").submit(function(e) {
    var url,data,json,post,button,form;
    url  = '<?php echo base_url();?>Admin/kelas/edit_json';
    data = $(this).serialize();
    form = $("#formEdit input");
    json = 'json';
    post = 'post';
    button = $("#btnedit");
    button.removeClass('btn-primary').addClass('btn-default');
    form.prop('disabled', true);
    event.preventDefault();
    setTimeout(function() {
        $.ajax({
            method   : post,
            url      : url,
            dataType : json,
            data     : data,
            success: function(res){
                if (res.status == 200) {
                    notif('success', 'Berhasil Mengubah Kelas');
                    $('#modal_kelasedit').modal('hide');
                    reload_table();
                    form.prop('disabled', false);
                    button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                    button.attr('disabled',false);

                }else if(! $.isEmptyObject(res.errors)){

                    notif('info', res.errors);
                    $('#modal_kelasedit').modal('hide');  
                    reload_table();
                    form.prop('disabled', false);
                    button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                    button.attr('disabled',false);
                }
            },
            error: function(){
                notif('info', 'Mengubah Kelas Error');
                $('#modal_kelasedit').modal('hide');  
                reload_table();
                console.log('500 | Ubah Kelas Error');
                button.removeClass('btn-default').addClass('btn-primary').text('Edit').prop('disabled', false);
                button.attr('disabled',false);
                form.prop('disabled', false);
            }
        })
    }, 210);
});

$("#tambah").click(function(event) {
    setTimeout(function(){
        ClearFormData("#formTambah");
$('#modal_kelas').modal('show'); // show bootstrap modal
}, 550);
});
$("#formTambah").submit(function(e) {
    var url,data,json,post,button,form;
    url = '<?php echo base_url();?>Admin/kelas/add_json';
    data = $(this).serialize();
    form = $("#formTambah input");
    json = 'json';
    post = 'post';
    button = $("#btntambah");
    button.removeClass('btn-primary').addClass('btn-default');
    form.prop('disabled', true);
    event.preventDefault();
    setTimeout(function() {
        $.ajax({
            method   : post,
            url      : url,
            dataType : json,
            data     : data,
            success: function(res){
                if (res.status == 200) {
                    notif('success', 'Berhasil Menambahkan Kelas');
                    $('#modal_kelas').modal('hide');
                    reload_table();
                    form.prop('disabled', false);
                    button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);

                }else if(! $.isEmptyObject(res.errors)){

                    notif('info', res.errors);
                    $('#modal_kelas').modal('hide'); 
                    reload_table(); 
                    form.prop('disabled', false);
                    button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                }
            },
            error: function(){
                notif('info', 'Menambah Kelas Error');
                $('#modal_kelas').modal('hide');  
                reload_table();
                console.log('500 | Tambah Kelas Error');
                button.removeClass('btn-default').addClass('btn-primary').text('Tambah').prop('disabled', false);
                form.prop('disabled', false);
            }
        })
    }, 210);
});


$("#btndelete").click(function(e) {
    var url,data,json,post,button;
    var id = $('#kelas_delete_id').val();
    url = '<?php echo base_url();?>Admin/kelas/delete_json';
    json = 'json';
    post = 'post';
    button = $("#btndelete");
    button.removeClass('btn-danger').addClass('btn-default');
    setTimeout(function() {
        $.ajax({
            method   : post,
            url      : url,
            dataType : json,
            data     : {id:id},
            success: function(res){
                if (res.status == 200) {
                    notif('success', 'Berhasil Menghapus Kelas');
                    $('#ModalHapus').modal('hide');  
                    reload_table();
                    button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);
                    
                }else if(res.status == 400){

                    notif('error', 'Gagal Menghapus Kelas');
                    $('#ModalHapus').modal('hide');  
                    reload_table();  
                    button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false);
                }
            },
            error: function(){
                notif('info', 'Menghapus Kelas Error');
                $('#ModalHapus').modal('hide');    
                reload_table();
                console.log('500 | Tambah Kelas Error');
                button.removeClass('btn-default').addClass('btn-danger').text('Delete').prop('disabled', false)
            }
        })
    }, 210);
});


function reload_table()
{
table.ajax.reload(null,false); //reload datatable ajax 
}


</script>
