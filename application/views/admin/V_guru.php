<script>

			function add_person()
		{
		    $('#validation')[0].reset(); // reset form on modals
		    $('#modalguru').modal('show'); // show bootstrap modal
		}

		function edit_guru(id)
		{
		$('#form')[0].reset(); // reset form on modals
		//Ajax Load data from ajax
		$.ajax({
			url : "<?php echo site_url('Admin/guru/get_by_id')?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
				$('[name="edit_id"]').val(data.id);
				$('[name="edit_nip"]').val(data.nip);
				$('[name="edit_nama"]').val(data.nama);
				$('[name="edit_jabatan"]').val(data.jabatan);
				$('[name="edit_alamat"]').val(data.alamat);
				$('[name="edit_email"]').val(data.email);
				$('#ModalEditGuru').modal('show');  
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		});
		} 

		function delete_guru(id)
		{
			$.ajax({
				url : "<?php echo site_url('Admin/guru/get_by_id')?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
					$('[name="delete_id"]').val(data.id);
					$('#ModalHapusGuru').modal('show');  
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error get data from ajax');
				}
			});
		} 
</script>

		<!-- start page content -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="page-bar">
					<div class="page-title-breadcrumb">
						<div class=" pull-left">
							<div class="page-title">Data Guru</div>
						</div>
						<ol class="breadcrumb page-breadcrumb pull-right">
							<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
								href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
							</li>
							<li class="active">Guru</li>
						</ol>
					</div>
				</div>

				<!-- start new student list -->

				<div class="row">
					<div class="col-md-10"></div>

					<div class="col-md-2">
						<button data-target="#modalguru" data-toggle="modal"class="btn btn-primary">Tambah Data</button>
					</div>

					<div class="col-md-12 col-sm-12">
						<div class="card card-topline-aqua">
							<div class="card-head">
								<header>Tabel Guru</header>
							</div>
							<div class="card-body ">
								<div class="table-wrap">
									<div class="table-responsive">
										<table width="100%" class="display guru" class="display">
											<thead>
												<tr>
													<th>No</th>
													<th>NIP</th>
													<th>Nama</th>
													<th>Agama</th>
													<th>Jabatan</th>
													<th>Tangal Lahir</th>
													<th>Jenis Kelamin</th>
													<th>Action</th>	
												</tr>
											</thead>
											<tbody>
												<tr>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- Modal tambah Guru -->
		<div class="modal fade" id="modalguru" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="validation">  
							<h4 class="text-center" style="padding-bottom: 15px">Tambah Guru</h4>
							<div class="row">
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;" for="text5">NIP </label>
									<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="add_nip" required>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Nama</label>
									<input class="mdl-textfield__input" type="text" required name="add_nama">
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Tanggal Lahir</label>
									<input class="mdl-textfield__input  formDatePicker" type="text" required name="add_tgl_lahir" id="dp1">
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Jabatan</label>
									<input class="mdl-textfield__input" type="text" required name="add_jabatan">
								</div>
							</div>
							<div class="col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Jenis Kelamin</label>
									<input class="mdl-textfield__input" type="text" id="gender" name="add_jk" value="" readonly tabIndex="-1">
										<label for="gender" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
										</label>
											<ul data-mdl-for="gender" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
											<li class="mdl-menu__item" data-val="Laki-laki">Laki laki</li>
											<li class="mdl-menu__item" data-val="Perempuan">Perempuan</li>
										</ul>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Email</label>									
								<input class="mdl-textfield__input" type="email" id="txtemail" name="add_email" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Agama</label>
									<input class="mdl-textfield__input" type="text" id="agama" name="add_agama" value="" readonly tabIndex="-1">
										<label for="agama" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
										</label>
											<ul data-mdl-for="agama" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
											<li class="mdl-menu__item" data-val="Islam">Islam</li>
											<li class="mdl-menu__item" data-val="Kristen">Kristen</li>
											<li class="mdl-menu__item" data-val="Hindu">Hindu</li>
										</ul>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Alamat</label>
									<textarea class="mdl-textfield__input" rows="1" id="text7" name="add_alamat" required></textarea>	
								</div>
							</div>
							</div>
							<br>
							<div class="col-lg-12 p-t-20 text-center">
								<button type="submit"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
								<button type="button"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--   End Modal -->

		<!-- Modal Edit Guru -->
		<div class="modal fade" id="ModalEditGuru" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="form">  
							<h4 class="text-center" style="padding-bottom: 15px">Edit Guru</h4>
							<div class="row">
							<input type="hidden" name="edit_id">
							<div class="form-group col-md-6">
								<label class="form-check-label" style="font-size: 11px;color:grey;">NIP</label>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="edit_nip" placeholder="NIP" required>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Nama</label>
									<input placeholder="Nama" class="mdl-textfield__input" type="text" id="txtFirstName" name="edit_nama" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Jenis Kelamin</label>
									<input class="mdl-textfield__input" type="text" id="gender" name="edit_jk" value="" readonly tabIndex="-1" required="">
										<label for="gender" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
										</label>
											<ul data-mdl-for="gender" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
											<li class="mdl-menu__item" data-val="Laki-laki">Laki laki</li>
											<li class="mdl-menu__item" data-val="Perempuan">Perempuan</li>
										</ul>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Jabatan</label>
									<input placeholder="Jabatan" class="mdl-textfield__input" type="text" id="txtFirstName" name="edit_jabatan" required>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Tanggal Lahir</label>
									<input class="mdl-textfield__input" type="text" name="edit_tgl_lahir" placeholder="Tanggal Lahir">
								</div>
							</div>
							<div class="col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Agama</label>
									<input class="mdl-textfield__input" type="text" id="agama" name="edit_agama" value="" readonly tabIndex="-1">
										<label for="agama" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
										</label>
											<ul data-mdl-for="agama" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
											<li class="mdl-menu__item" data-val="Islam">Islam</li>
											<li class="mdl-menu__item" data-val="Kristen">Kristen</li>
											<li class="mdl-menu__item" data-val="Hindu">Hindu</li>
										</ul>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Email</label>
								<input class="mdl-textfield__input" type="email" required id="txtemail" name="edit_email">
								</div>
							</div>
							<div class="form-group col-md-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Alamat</label>
									<textarea class="mdl-textfield__input" rows="1" required id="text7" name="edit_alamat"></textarea>	
								</div>
							</div>
							</div>
							<br>
							<div class="col-lg-12 p-t-20 text-center">
								<button type="submit"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
								<button type="button"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Edit guru  -->

		<!-- Hapus guru -->
		<div id="ModalHapusGuru" class="modal fade">
		  <div class="modal-dialog modal-confirm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <div class="icon-box">
		          <i class="material-icons">&#xE5CD;</i>
		        </div>
		      </div>
		      <div class="modal-body">
		        <input type="hidden" name="delete_id">
		        <p>Apakah anda yakin data ingin di hapus?</p>
		      </div>
		      <div class="model-footer">
		      <div class="row">
		    	<div class="col-md-2"></div>
		    	<div class="col-md-8">
		        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
		        <button type="button" class="btn btn-danger" id="hapus">Delete</button>
		        </div>
		      	<div class="col-md-2"></div>
		      </div>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- End Hapus -->


<script src="<?php echo config_item('js');?>jquery.min.js"></script>
<script>

	var table;

$(document).ready(function() {

    //datatables
    table = $('.guru').DataTable({ 

        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/guru/guru_list_json')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        });

    	$("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});

  $("#validation").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/guru/add_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method   : post,
    url      : url,
    dataType : json,
    data     : data,
    success : function(data){
    if(data.status) //if success close modal and reload ajax table
    {
    	$('#validation').each(function(){
		this.reset();});
        $('#modalguru').modal('hide');
        reload_table();
    }
    },
       error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
  });
  return false;
});



  $("#form").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/guru/edit_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method : post,
    url    : url,
    dataType : json,
    data : data,
    success : function(data){
    $('[name="edit_id"]').val("");
    $('[name="edit_nip"]').val("");
	$('[name="edit_nama"]').val("");
    $('[name="edit_jabatan"]').val("");
	$('[name="edit_alamat"]').val("");
    $('[name="edit_email"]').val("");
	$('[name="edit_agama"]').val("");
	$('[name="edit_tgl_lahir"]').val("");
	$('[name="edit_jk"]').val("");
    $('#ModalEditGuru').modal('hide');  
    alert("Berhasil");
    reload_table();    
    },
    error: function (jqXHR,textStatus,data,errorThrown)
  {
  	$('[name="edit_id"]').val("");
	$('[name="edit_nip"]').val("");
	$('[name="edit_nama"]').val("");
    $('[name="edit_jabatan"]').val("");
	$('[name="edit_alamat"]').val("");
    $('[name="edit_email"]').val("");
	$('[name="edit_agama"]').val("");
	$('[name="edit_tgl_lahir"]').val("");
	$('[name="edit_jk"]').val("");
    $('#ModalEditGuru').modal('hide');  
    alert("Gagal");
    reload_table();    
  }
  });
  return false;
});


	  $('#hapus').on('click',function(){
	  	var delete_id=$('[name="delete_id"]').val();
	  	$.ajax({
	  		type : "POST",
	  		url  : "<?php echo base_url('Admin/guru/delete_json')?>",
	  		dataType : "JSON",
	  		data :{delete_id:delete_id},
	  		success: function(data){
	  			alert("Data Berhasil dihapus");
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusGuru').modal('hide');                     
	  		},
	  		error: function (jqXHR,textStatus,data,errorThrown)
	  		{
	  			alert("Data Gagal dihapus");
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusGuru').modal('hide');                     
	  		}

	  	});
	  	return false;
	  });




  function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

		</script>
