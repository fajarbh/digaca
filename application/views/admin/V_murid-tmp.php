<script>
			function add_person()
		{
		    $('#validation')[0].reset(); // reset form on modals
		    $('#ModalMurid').modal('show'); // show bootstrap modal
		}

		function edit_murid(id)
		{
		$('#form')[0].reset(); // reset form on modals
		//Ajax Load data from ajax
		$.ajax({
			url : "<?php echo site_url('Admin/murid/get_by_id')?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
				$('[name="edit_id"]').val(data.id);
				$('[name="edit_nis"]').val(data.nis);
				$('[name="edit_nama"]').val(data.nama);
				$('[name="edit_jk"]').val(data.jenis_kelamin);
				$('[name="edit_tgl_lhr"]').val(data.tgl_lahir);
				$('[name="edit_nohp"]').val(data.no_hp);
				$('[name="edit_email"]').val(data.email);
				$('[name="edit_agama"]').val(data.agama);
				$('[name="edit_alamat"]').val(data.alamat);
				
				$('#ModalEditMurid').modal('show');  
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
			}
		});
		} 

		function delete_murid(id)
		{
			$.ajax({
				url : "<?php echo site_url('Admin/murid/get_by_id')?>/"+id,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
					$('[name="delete_id"]').val(data.id);
					$('#ModalHapusMurid').modal('show');  
				},
				error: function (jqXHR, textStatus, errorThrown)
				{

				}
			});
		} 
</script>

		<!-- start page content -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="page-bar">
					<div class="page-title-breadcrumb">
						<div class=" pull-left">
							<div class="page-title">Data Murid</div>
						</div>
						<ol class="breadcrumb page-breadcrumb pull-right">
							<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
								href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
							</li>
							<li class="active">Murid</li>
						</ol>
					</div>
				</div>

				<!-- start new student list -->

				<div class="row">
					<div class="col-md-10"></div>

					<div class="col-md-2">
						<button data-target="#ModalMurid" data-toggle="modal"class="btn btn-primary">Tambah Data</button>
					</div>

					<div class="col-lg-12 col-sm-12">
						<div class="card card-topline-aqua">
							<div class="card-head">
								<header>Tabel Murid</header>
							</div>
							<div class="card-body ">
								<div class="table-wrap">
									<div class="table-responsive">
										<table width="100%" class="display Murid" class="display">
											<thead>
												<tr>
													<th>No</th>
													<th>Nis</th>
													<th>Nama</th>
													<th>Kelas</th>
													<th>Jenis Kelamin</th>
													<th>Tanggal Lahir</th>
													<th>Agama</th>
													<th>Action</th>	
												</tr>
											</thead>
											<tbody>
												<tr>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- Modal tambah Murid -->
		<div class="modal fade" id="ModalMurid" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg  modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="validation">  
							<h4 class="text-center" style="padding-bottom: 15px">Tambah Murid</h4>
							<div class="row">
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">NIS</label>
									<input class="mdl-textfield__input" type="text" autocomplete="off" autosave="false" required name="add_nis">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Nama Murid</label>
									<input class="mdl-textfield__input" type="text" autocomplete="off" autosave="false" required name="add_nama">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Jenis Kelamin</label>
									<input class="mdl-textfield__input" type="text" id="gender" name="add_jk" value=""
										readonly tabIndex="-1">
									<label for="gender" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
									</label>
									<ul data-mdl-for="gender"
										class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
										<li class="mdl-menu__item" data-val="Pria">Pria</li>
										<li class="mdl-menu__item" data-val="Wanita">Wanita</li>
									</ul>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Tanggal Lahir</label>
									<input class="mdl-textfield__input formDatePicker" type="text" name="add_tgl_lhr" id="dp1">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Agama</label>
									<input class="mdl-textfield__input" type="text" required="" name="add_agama">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">No HP</label>
									<input class="mdl-textfield__input" type="text" name="add_nohp" value=""
										pattern="-?[0-9]*(\.[0-9]+)?" id="text5" autocomplete="off" autosave="false" required>
									<span class="mdl-textfield__error">Number required!</span>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Email</label>
									<input class="mdl-textfield__input" type="email" name="add_email" value=""
										id="txtemail">
									<span class="mdl-textfield__error">Enter Valid Email Address!</span>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Alamat</label>
									<textarea class="mdl-textfield__input" rows="1" 
									 name="add_alamat"></textarea>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="col-lg-12 p-t-20 text-center">
								<button type="button"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
								<button type="button"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--   End Modal -->

		<!-- Hapus Murid -->
		<div id="ModalHapusMurid" class="modal fade">
		  <div class="modal-dialog modal-confirm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <div class="icon-box">
		          <i class="material-icons">&#xE5CD;</i>
		        </div>
		      </div>
		      <div class="modal-body">
		        <input type="hidden" name="delete_id">
		        <p>Apakah anda yakin data ingin di hapus?</p>
		      </div>
		      <div class="model-footer">
		      <div class="row">
		    	<div class="col-md-2"></div>
		    	<div class="col-md-8">
		        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
		        <button type="button" class="btn btn-danger" id="hapus">Delete</button>
		        </div>
		      	<div class="col-md-2"></div>
		      </div>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- End Hapus -->

		<!-- Modal Edit Murid -->
		<div class="modal fade" id="ModalEditMurid" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-body">
						<form id="form">  
							<h4 class="text-center" style="padding-bottom: 15px">Edit Murid</h4>
							<input type="hidden" name="edit_id">
							<div class="row">
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Nis Murid</label>
									<input class="mdl-textfield__input" type="text" required="" name="edit_nis" value="edit_nis">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Nama Murid</label>
									<input class="mdl-textfield__input" type="text" required="" name="edit_nama" value="edit_nama">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
								<label class="form-check-label" style="font-size: 11px;color:grey;">Jenis Kelamin</label>
									<input class="mdl-textfield__input" type="text" id="gender" name="edit_jk" 
										readonly tabIndex="-1" value="edit_jk" autocomplete="off" autosave="false" required>
									<label for="gender" class="pull-right margin-0">
										<i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
									</label>
									<ul data-mdl-for="gender"
										class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
										<li class="mdl-menu__item" data-val="Pria">Pria</li>
										<li class="mdl-menu__item" data-val="Wanita">Wanita</li>
									</ul>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								    <label class="form-check-label" style="font-size: 11px;color:grey;">Tanggal Lahir</label>
									<input class="mdl-textfield__input formDatePicker" type="text" name="edit_tgl_lhr" value="edit_tgl_lahir" id="dp1">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 11px;color:grey;">Agama</label>
									<input class="mdl-textfield__input" type="text" required="" name="edit_agama" value="edit_agama">
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
									<label class="form-check-label" style="font-size: 12px;color:grey;">No HP</label>
									<input class="mdl-textfield__input" type="text" name="edit_nohp" 
										pattern="-?[0-9]*(\.[0-9]+)?" id="text5" value="1">
									<span class="mdl-textfield__error">Number required!</span>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
											<label class="form-check-label" style="font-size: 11px;color:grey;">Email</label>
									<input class="mdl-textfield__input" type="email" name="edit_email" 
										id="txtemail" value="q@gmail" required="">
									<span class="mdl-textfield__error">Enter Valid Email Address!</span>
								</div>
							</div>
				<!-- ========================================================================================= -->
							<div class="form-group col-md-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width ">
											<label class="form-check-label" style="font-size: 11px;color:grey;">Alamat</label>
									<textarea class="mdl-textfield__input" rows="1"
									 name="edit_alamat" required="">1</textarea>
								</div>
							</div>
				<!-- ========================================================================================= -->

							<br>
							<div class="col-lg-12 p-t-20 text-center">
								<button type="submit"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
								<button type="button"
									class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Edit Kelas  -->


<script src="<?php echo config_item('js');?>jquery.min.js"></script>
<script>

	var table;

$(document).ready(function() {

    //datatables
    table = $('.Murid').DataTable({ 

        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/murid/murid_list_json')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        });

    	$("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});

  $("#validation").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/murid/add_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method   : post,
    url      : url,
    dataType : json,
    data     : data,
    success : function(data){
    if(data.status) //if success close modal and reload ajax table
    {
    	$('#validation').each(function(){
		this.reset();});
        $('#ModalMurid').modal('hide');
        reload_table();
    }
    },
       error: function (jqXHR, textStatus, errorThrown)
        {
       
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
  });
  return false;
});



  $("#form").submit(function(e) {
  var url,data,json,post;
  json = 'json';
  post = 'post';
  url  = '<?php echo base_url();?>Admin/murid/edit_json';
  data = $(this).serialize();   
  e.preventDefault();
  $.ajax({
    method : post,
    url    : url,
    dataType : json,
    data : data,
    success : function(data){
    $('[name="edit_id"]').val("");
	$('[name="edit_nis"]').val("");
	$('[name="edit_nama"]').val("");
	$('[name="edit_nis"]').val("");
	$('[name="edit_jk"]').val("");
	$('[name="edit_tgl_lhr"]').val("");
	$('[name="edit_nohp"]').val("");
	$('[name="edit_email"]').val("");
	$('[name="edit_agama"]').val("");
    $('#ModalEditMurid').modal('hide');  
    reload_table();    
    },
    error: function (jqXHR,textStatus,data,errorThrown)
  {
  	$('[name="edit_id"]').val("");
	$('[name="edit_nis"]').val("");
	$('[name="edit_nama"]').val("");
	$('[name="edit_nis"]').val("");
	$('[name="edit_jk"]').val("");
	$('[name="edit_tgl_lhr"]').val("");
	$('[name="edit_nohp"]').val("");
	$('[name="edit_email"]').val("");
	$('[name="edit_agama"]').val("");
    $('#ModalEditMurid').modal('hide');  
    reload_table();    
  }
  });
  return false;
});


	  $('#hapus').on('click',function(){
	  	var delete_id=$('[name="delete_id"]').val();
	  	$.ajax({
	  		type : "POST",
	  		url  : "<?php echo base_url('Admin/murid/delete_json')?>",
	  		dataType : "JSON",
	  		data :{delete_id:delete_id},
	  		success: function(data){
	
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusMurid').modal('hide');                     
	  		},
	  		error: function (jqXHR,textStatus,data,errorThrown)
	  		{
	
	  			$('[name="delete_id"]').val("");
	  			reload_table();
	  			$('#ModalHapusMurid').modal('hide');                     
	  		}

	  	});
	  	return false;
	  });




  function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

		</script>