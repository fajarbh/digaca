<?php 

require_once('Google/autoload.php');
class Google {
    protected $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->config->load('google');
        $this->client = new Google_Client();
        $this->client->setClientId($this->CI->config->item('google_client_id'));
        $this->client->setClientSecret($this->CI->config->item('google_client_secret'));
         $this->client->setRedirectUri($this->CI->config->item('google_redirect_url'));
        $this->client->setScopes(array(
            "https://www.googleapis.com/auth/plus.login",
            "https://www.googleapis.com/auth/plus.me",
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/userinfo.profile"
            )
        );
  

    }


    public function validate(){     
        if (isset($_GET['code'])) {
          $this->client->authenticate($_GET['code']);
          $_SESSION['access_token'] = $accessToken = $this->client->getAccessToken();
        }
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
          $accessToken = $this->client->setAccessToken($_SESSION['access_token']);
          $plus = new Google_Service_Plus($this->client);
            $person = $plus->people->get('me');
                $info['oauth_uid']      = !empty($person['id'])?$person['id']:'';;
                $info['first_name']     = $person['displayName'];
                $info['last_name']      = 'Last Name';
                $info['email']          = $person['emails'][0]['value'];
                $info['picture']        = substr($person['image']['url'],0,strpos($person['image']['url'],"?sz=50")) . '?sz=800';
                $info['link']           = !empty($person['url'])?$person['url']:'';
           return  $info;
        }


    }

    public function get_login_url(){
        return  $this->client->createAuthUrl();
    }

        public function revokeToken(){
        return  $this->client->revokeToken($_SESSION['access_token']);;
    }
    

}