<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
   
        public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('M_auth','auth',TRUE);
        $this->load->library('facebook');
        $this->load->model('M_admin');
        $this->load->model('M_kelas');
        $this->load->model('M_murid');
        $this->load->model('M_mapel');
        $this->checkAuth();
    }

        public function checkAuth() {
        if (!$this->session->userdata('backToken')) {
            redirect('Siswa/login');
        } else {
            if (!$this->auth->checkToken($this->session->userdata('backToken'))) {
                redirect('Siswa/logout');
            }
        }
    }

        public function message($title = NULL,$text = NULL,$type = NULL) {
        return $this->session->set_flashdata([
                'title' => $title,
                'text' => $text,
                'type' => $type
            ]
        );
    }
    
    public function getStudentByNis($nis) {
    $this->db->where('student_nis', $nis);
    $this->db->where('student_hide', 0);
    return $this->db->get('ms_student')->row_object();
    }

    



  }