<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {


    public function selectBy($tbl,$data)
        {
          return $this->db->get_where($tbl,$data);
        }

      public function getUserByEmail($email) {
        $this->db->where('email', $email);
        $this->db->where('is_active', '1');
        return $this->db->get('users');
      }

      public function getAdminByUsername($username) {
        $this->db->where('username', $username);
        $this->db->where('active', 1);
        return $this->db->get('admin');
      }

      public function getTeacherByUsername($username) {
        $this->db->where('username', $username);
        $this->db->where('active', 1);
        return $this->db->get('guru');
      }

      public function checkToken($access_token) {
      $this->db->where('access_token', $access_token);
      return $this->db->get('auth_token')->row_object();
      } 
      public function registToken($data) {
      return $this->db->insert('auth_token', $data);
      }
      public function deleteToken($access_token) {
      $this->db->where('access_token', $access_token);
      return $this->db->delete('auth_token');
    }    

}

/* End of file M_auth.php */
/* Location: ./application/models/M_auth.php */