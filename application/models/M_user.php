<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {


    function __construct() {
        $this->tableName = 'users';
        $this->primaryKey = 'id_user';
        $this->load->database();
    }
      
          public function save($data)
        {
          $this->db->insert($this->table, $data);
          return $this->db->insert_id();
        }
      
        public function update($where, $data)
        {
          $this->db->update($this->table, $data, $where);
          return $this->db->affected_rows();
        }
      
         public function delete($id)
        {
          $hasil=$this->db->query("DELETE FROM kelas WHERE id_user='$id'");
          return $hasil;
        }
      
          public function get_by_id($id)
        {
          $this->db->from($this->table);
          $this->db->where('id_user',$id);
          $query = $this->db->get();
          return $query->row();
        }   
    //End
     public function checkUser($userData = array()){
        if(!empty($userData)){
            //check whether user data already exists in database with same oauth info
            $this->db->select($this->primaryKey);
            $this->db->from($this->tableName);
            $this->db->where(array('oauth_provider'=>$userData['oauth_provider'], 'oauth_uid'=>$userData['oauth_uid']));
            $prevQuery = $this->db->get();
            $prevCheck = $prevQuery->num_rows();
            
            if($prevCheck > 0){
                $prevResult = $prevQuery->row_array();
                
                //update user data
                $update = $this->db->update($this->tableName, $userData, array('id_user' => $prevResult['id_user']));
                
                //get user ID
                $userID = $prevResult['id_user'];
            }else{
                //insert user data
                $insert = $this->db->insert($this->tableName, $userData);
                
                //get user ID
                $userID = $this->db->insert_id();
            }
        }
        return $userID?$userID:FALSE;
    }

}