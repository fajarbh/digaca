<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_kelas extends CI_Model {

  var $table = "kelas";

  function getAllData()
  {
    return $this->db->get($this->table);
  }

    public function save($data)
  {
   $insert = $this->db->insert($this->table, $data);
   if ($insert) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

  public function update($where, $data)
  {
   $update = $this->db->update($this->table, $data, $where);
   if ($update) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

   public function delete($id)
  {    
    $this->db->where('id', $id);
		$delete = $this->db->delete($this->table);
    if ($delete) {
      return ['res' => 200];     
     }else{
      return ['res' => 400];
     }
  }

    public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function getAll()
  {
      $this->db->select('*');
      $this->db->from('kelas');
      $query = $this->db->get();
      return $query->result();
  }

}

/* End of file M_kelas.php */
/* Location: ./application/models/M_kelas.php */
