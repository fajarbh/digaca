<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_nilai extends CI_Model {

  var $table = "nilai_siswa";

  function getAllData()
  {
    return $this->db->get($this->table);
  }

  public function Getdata()
  {
    $this->db->select('*');
    $this->db->from('nilai_siswa');
    $this->db->join('mapel', 'mapel.kode_mapel = nilai_siswa.kode_mapel');
    $this->db->join('semester','semester.id_semester = nilai_siswa.semester_id');
    $this->db->join('murid','murid.id_murid = nilai_siswa.murid_id');
    return $query = $this->db->get();
  }

    public function save($data)
  {
   $insert = $this->db->insert($this->table, $data);
   if ($insert) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

  public function update($where, $data)
  {
   $update = $this->db->update($this->table, $data, $where);
   if ($update) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

   public function delete($id)
  {    
    $this->db->where('id', $id);
		$delete = $this->db->delete($this->table);
    if ($delete) {
      return ['res' => 200];     
     }else{
      return ['res' => 400];
     }
  }

    public function get_by_id($id)
  {
    $this->db->select('*');
    $this->db->from('nilai_siswa');
    $this->db->join('mapel', 'mapel.kode_mapel = nilai_siswa.kode_mapel');
    $this->db->join('semester','semester.id_semester = nilai_siswa.semester_id');
    $this->db->where('id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function getAll()
  {
      $this->db->select('*');
      $this->db->from('kelas');
      $query = $this->db->get();
      return $query->result();
  }

}

/* End of file M_nilai.php */
/* Location: ./application/models/M_nilai.php */
