<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_guru extends CI_Model {

  var $table = "guru";

  function getAllData()
  {
    return $this->db->get($this->table);
  }

  function get_kelas(){
    $hasil=$this->db->query("SELECT * FROM kelas");
    return $hasil;
  }
  function get_kode_mapel(){
    $hasil=$this->db->query("SELECT * FROM mapel");
    return $hasil;
  }
  function getAllthnAjaran()
  {
    return $this->db->get("semester")->result();
  }
  function getAllSemester()
  {
    return $this->db->get("semester")->result();
  }

  function get_subnama($id){
    $this->db->where('murid_kelas',$id);
    $hasil = $this->db->get('murid');
    return $hasil->result();
  }

    public function save($data)
  {
   $insert = $this->db->insert($this->table, $data);
   if ($insert) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

  public function update($where, $data)
  {
   $update = $this->db->update($this->table, $data, $where);
   if ($update) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

   public function delete($id)
  {
    $this->db->where('id', $id);
		$delete = $this->db->delete($this->table);
    if ($delete) {
      return ['res' => 200];
     }else{
      return ['res' => 400];
     }
  }

    public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function getAll()
  {
      $this->db->select('*');
      $this->db->from('mapel');
      $query = $this->db->get();
      return $query->result();
  }

}

/* End of file M_kelas.php */
/* Location: ./application/models/M_kelas.php */
