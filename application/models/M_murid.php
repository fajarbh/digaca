<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_murid extends CI_Model {

  	var $table = "murid";
	  function getAllData()
  {
      $this->db->select('*');
      $this->db->from($this->table);
      $this->db->join('kelas', 'kelas.kelas = murid.murid_kelas');
      $query = $this->db->get();
      return $query->result();
  }

  public function get_jml_murid(){
		$query='SELECT murid_kelas, count(*) as jml FROM murid group by murid_kelas';
		return $this->db->query($query)->result();
	}

    public function save($data)
  {
   $insert = $this->db->insert($this->table, $data);
   if ($insert) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

  public function update($where, $data)
  {
   $update = $this->db->update($this->table, $data, $where);
   if ($update) {
    return ['res' => 200];
   }else{
    return ['res' => 400];
   }
  }

   public function delete($id)
  {
    $this->db->where('id', $id);
		$delete = $this->db->delete($this->table);
    if ($delete) {
      return ['res' => 200];
     }else{
      return ['res' => 400];
     }
  }

    public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function getAll()
  {
      $this->db->select('*');
      $this->db->from('kelas');
      $query = $this->db->get();
      return $query->result();
  }

	public function insertimport($data)
    {
        $this->db->insert_batch('murid', $data);
        return $this->db->insert_id();
    }
}

/* End of file M_murid.php */
/* Location: ./application/models/M_murid.php */
