<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruController extends CI_Controller {



	function __construct(){
		parent::__construct();
		$this->load->model('M_guru');
		if (!$this->session->userdata('GuruToken')) {
                $this->session->sess_destroy();
                redirect('Siswa/login');
	}
}

	public function get_subnama(){
            $id=$this->input->get('id');
            $data=$this->M_guru->get_subnama($id);
            echo json_encode($data);
     }

    public function index()
	{
		$title['nav_title'] = 'Dashboard';
		$this->load->view('frontend/V_headerGuru',$title);
		$this->load->view('guru/V_dashboard');
		$this->load->view('frontend/V_footerGuru');
	}

	public function jadwalpelajaran()
	{
		$title['nav_title'] = 'Jadwal Pelajaran';
		$this->load->view('frontend/V_headerGuru',$title);
		$this->load->view('guru/Jadwalpelajaran/V_jadwalpelajaran');
		$this->load->view('frontend/V_footerGuru');
	}

	public function nilaisiswa()
	{
		$title['nav_title'] = 'Nilai Siswa';
		$x['kode_mapel']=$this->M_guru->get_kode_mapel();
		$x['data']=$this->M_guru->get_kelas();
		$x['thn_ajaran'] = $this->M_guru->getAllthnAjaran();
		$x['semester'] = $this->M_guru->getAllSemester();
		$this->load->view('frontend/V_headerGuru',$title);
		$this->load->view('guru/Nilaisiswa/V_nilaisiswa',$x);
		$this->load->view('guru/Nilaisiswa/addjs');
		$this->load->view('frontend/V_footerGuru');
	}
}