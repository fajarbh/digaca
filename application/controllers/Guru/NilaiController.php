<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiController extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('M_nilai');
                if (!$this->session->userdata('GuruToken')) {
                $this->session->sess_destroy();
                redirect('Siswa/login');
        }
    }

        public function nilai_list_json()
        {
                $datanilai = $this->M_nilai->Getdata()->result();
                $no = 1;
                foreach ($datanilai as $nilai) {
                        $tbody = array();
                        $tbody[] = $no++;
                        $tbody[] = $nilai->nama;
                        $tbody[] = $nilai->nama_mapel; 
                        $tbody[] = $nilai->thn_ajaran;
                        $tbody[] = $nilai->uts;
                        $tbody[] = $nilai->uas;                       
                        $tbody[] = $nilai->pengetahuan;
                        $tbody[] = $nilai->keterampilan;
                        $tbody[] = $nilai->kkm;
                        $tbody[] = $nilai->nama_semester;
                        $aksi    = '<a class="" href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_nilai('."'".$nilai->id_nilai_siswa."'".')"><i class="fa fa-check text-primary"></i></a>
                        <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_nilai('."'".$nilai->id_nilai_siswa."'".')"><i class="fa fa-trash text-primary"></i></a>';
                        $tbody[] = $aksi;
                        $data[]  = $tbody;
                }
                if ($datanilai) {
                        echo json_encode(array('data' => $data));
                }else{
                        echo json_encode(array('data' => 0));
                }
        }

        public function add_json()
        {
                $config = array(
                        array(
                                'field'  => 'add_namasiswa',
                                'label'  => 'Nama Siswa',
                                'rules'  => 'required|trim|min_length[1]|max_length[50]',                                
                        ),
                        array(
                                'field'  => 'add_kode_mapel',
                                'label'  => 'Kode Mapel',
                                'rules'  => 'required|trim|min_length[1]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_thn_ajaran',
                                'label'  => 'Tahun AJaran',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_nilai_uts',
                                'label'  => 'Nilai UTS',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_nilai_uas',
                                'label'  => 'Nilai UAS',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_nilai_pengetahuan',
                                'label'  => 'Nilai Pengetahuan',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_nilai_keterampilan',
                                'label'  => 'Nilai Keterampilan',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_kkm',
                                'label'  => 'KKM',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'add_semester',
                                'label'  => 'Semester',
                                'rules'  => 'required|trim|min_length[1]|max_length[20]'
                        )
                );
                $this->form_validation->set_rules($config);
                if( ! $this->form_validation->run()){
                        $error = validation_errors();
                        $return['errors'] = $error;
                }else{
                        $data = array(
                                'murid_id' => $this->input->post('add_namasiswa'),
                                'kode_mapel' => $this->input->post('add_kode_mapel'),
                                'thn_ajaran' => $this->input->post('add_thn_ajaran'),
                                'uts' => $this->input->post('add_nilai_uts'),
                                'uas' => $this->input->post('add_nilai_uas'),
                                'pengetahuan' => $this->input->post('add_nilai_pengetahuan'),
                                'keterampilan' => $this->input->post('add_nilai_keterampilan'),
                                'kkm' => $this->input->post('add_kkm'),
                                'semester_id' => $this->input->post('add_semester'),
                        );
                        $data = $this->security->xss_clean($data);
                        $insert = $this->M_nilai->save($data);

                        $return['status'] = $insert['res'];
                }

                echo json_encode($return);
                exit;

        }

        public function edit_json()
        {

               $config = array(
                        array(
                                'field'  => 'edit_nama',
                                'label'  => 'Nama Siswa',
                                'rules'  => 'required|trim|min_length[2]|max_length[50]',                                
                        ),
                        array(
                                'field'  => 'edit_kode',
                                'label'  => 'Kode Mapel',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_thn_ajaran',
                                'label'  => 'Tahun AJaran',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_uts',
                                'label'  => 'Nilai UTS',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_uas',
                                'label'  => 'Nilai UAS',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_pengetahuan',
                                'label'  => 'Nilai Pengetahuan',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_keterampilan',
                                'label'  => 'Nilai Keterampilan',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_kkm',
                                'label'  => 'KKM',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        ),
                        array(
                                'field'  => 'edit_semester',
                                'label'  => 'Semester',
                                'rules'  => 'required|trim|min_length[1]|max_length[20]'
                        )
                );
                $this->form_validation->set_rules($config);
                $this->form_validation->set_rules($config);
                if( ! $this->form_validation->run()){
                        $error = validation_errors();
                        $return['errors'] = $error;
                }else{
                        $data = array(
                                'nama_murid' => $this->input->post('edit_nama'),
                                'kode_mapel' => $this->input->post('edit_kode'),
                                'thn_ajaran' => $this->input->post('edit_thn_ajaran'),
                                'uts' => $this->input->post('edit_uts'),
                                'uas' => $this->input->post('edit_uas'),
                                'pengetahuan' => $this->input->post('edit_pengetahuan'),
                                'keterampilan' => $this->input->post('edit_keterampilan'),
                                'kkm' => $this->input->post('edit_kkm'),
                                'semester_id' => $this->input->post('edit_semester'),
                        );
                        $update = $this->M_nilai->update(array('id' => $this->input->post('edit_id')), $data);
                        $return['status'] = $update['res'];
                }
                echo json_encode($return);
        }

        public function delete_json(){
                $id = $this->input->post('id');
                $delete = $this->M_nilai->delete($id);
                $return['status'] = $delete['res'];
                echo json_encode($return);
        }

        public function get_by_id($id)
        {
                $res = $this->M_nilai->get_by_id($id);
                echo json_encode($res);
        }
  }

/* End of file KelasController.php */
/* Location: ./application/controllers/Admin/KelasController.php */
