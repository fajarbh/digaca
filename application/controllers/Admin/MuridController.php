<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MuridController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_murid');

	}

		public function murid_list_json()
	{
		$datamurid = $this->M_murid->getAllData();
        $no = 1;
        foreach ($datamurid as $murid) {
        	$tbody = array();
        	$tbody[] = $no++;
        	$tbody[] = $murid->nis;
        	$tbody[] = $murid->nama;
        	$tbody[] = $murid->jenis_kelamin;
        	$tbody[] = $murid->kelas;
        	$tbody[] = $murid->tahun_ajaran;
        	$aksi    = '<a class="" href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_murid('."'".$murid->id."'".')"><i class="fa fa-check text-primary"></i></a>
                  <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_murid('."'".$murid->id."'".')"><i class="fa fa-trash text-primary"></i></a>';
            $tbody[] = $aksi;
            $data[]  = $tbody;
        }
        if ($datamurid) {
        	echo json_encode(array('data' => $data));
        }else{
        	echo json_encode(array('data' => 0));
        }
	}

	public function add_json()
	{
		$config = array(
        array(
                'field'  => 'add_nis',
                'label'  => 'NIS',
                'rules'  => 'required|trim|min_length[6]|max_length[12]|is_unique[murid.nis]',
                'errors' => array(
                'is_unique'     => 'NIS telah terdaftar'
                ),
        ),

         array(
                'field'  => 'add_nama',
                'label'  => 'Nama Murid',
                'rules'  => 'required|trim|min_length[5]|max_length[24]'
        ),

	    array(
			    'field'  => 'add_thn_ajaran',
			    'label'  => 'Tahun Ajaran',
			    'rules'  => 'required|trim'
        ),

	    array(
			    'field'  => 'add_jk',
			    'label'  => 'Jenis Kelamin',
			    'rules'  => 'trim'
        ),

        array(
			    'field'  => 'add_tgl_lhr',
			    'label'  => 'Tanggal Lahir',
			    'rules'  => 'trim'
        ),

        array(
			    'field'  => 'add_agama',
			    'label'  => 'Agama',
			    'rules'  => 'trim|min_length[5]|max_length[24]'
        ),

	    array(
			    'field'  => 'add_nohp',
			    'label'  => 'No HP',
			    'rules'  => 'trim|min_length[6]|max_length[12]'
        ),

        array(
			    'field'  => 'add_email',
			    'label'  => 'Email',
			    'rules'  => 'trim|min_length[5]|max_length[24]'
        )

		);
		$this->form_validation->set_rules($config);
		if( ! $this->form_validation->run()){
		$error = validation_errors();
		$return['errors'] = $error;
		}else{
		$data = array(
		'nis'			  => $this->input->post('add_nis'),
	   	'nama' 			  => $this->input->post('add_nama'),
	   	'murid_kelas' 	  => $this->input->post('add_kelas'),
	   	'tahun_ajaran'    => $this->input->post('add_thn_ajaran'),
	   	'jenis_kelamin'	  => $this->input->post('add_jk'),
	   	'tgl_lahir'		  => $this->input->post('add_tgl_lhr'),
	   	'no_hp'			  => $this->input->post('add_nohp'),
	   	'email'			  => $this->input->post('add_email'),
	   	'agama'			  => $this->input->post('add_agama')
		);
		$insert = $this->M_murid->save($data);
		$return['status'] = $insert['res'];
		}

		echo json_encode($return);
		exit;

	}
	   public function edit_json()
	   {
	   	date_default_timezone_set('ASIA/JAKARTA');
	   	$data = array(
	   	'nis'			=> $this->input->post('edit_nis'),
	   	'nama' 			=> $this->input->post('edit_nama'),
	   	'jenis_kelamin' => $this->input->post('edit_jk'),
	   	'tgl_lahir'		=> $this->input->post('edit_tgl_lhr'),
	   	'no_hp'			=> $this->input->post('edit_nohp'),
	   	'email'			=> $this->input->post('edit_email'),
	   	'agama'			=> $this->input->post('edit_agama'),
	   	'alamat'		=> $this->input->post('edit_alamat'),
	   	);
	   	$this->M_murid->update(array('id' => $this->input->post('edit_id')), $data);
	   	echo json_encode(array("status" => TRUE));
	   }

	   public function delete_json(){
	   	$id = $this->input->post('delete_id');
	   	$result = $this->M_murid->delete($id);
	   	echo json_encode($result);
	   }

	   public function get_by_id($id)
      {
        $data = $this->M_murid->get_by_id($id);
        echo json_encode($data);
      }		    		
}
