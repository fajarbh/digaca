<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelasController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kelas');
	}

	public function kelas_list_json()
	{
		$datakelas = $this->M_kelas->getAllData()->result();
        $no = 1;
        foreach ($datakelas as $kelas) {
        	$tbody = array();
        	$tbody[] = $no++;
        	$tbody[] = $kelas->kelas;
        	$aksi    = '<a class="" href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_kelas('."'".$kelas->id."'".')"><i class="fa fa-check text-primary"></i></a>
                  <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_kelas('."'".$kelas->id."'".')"><i class="fa fa-trash text-primary"></i></a>';
            $tbody[] = $aksi;
            $data[]  = $tbody;
        }
        if ($datakelas) {
        	echo json_encode(array('data' => $data));
        }else{
        	echo json_encode(array('data' => 0));
        }
	}

	public function add_json()
	{
		$config = array(
        array(
                'field'  => 'add_kelas',
                'label'  => 'Kelas',
                'rules'  => 'required|trim|min_length[2]|max_length[24]|is_unique[kelas.kelas]',
                'errors' => array(
                'is_unique'     => 'Kelas telah terdaftar'
                ),
        )
		);
		$this->form_validation->set_rules($config);
		if( ! $this->form_validation->run()){
			$error = validation_errors();
			$return['errors'] = $error;
		}else{
		$data = array(
		'kelas' => $this->input->post('add_kelas'),
		);
		$data = $this->security->xss_clean($data);
		$insert = $this->M_kelas->save($data);

		$return['status'] = $insert['res'];
		}

		echo json_encode($return);
		exit;

	}

	public function edit_json()
	{

		$config = array(
        array(
                'field'  => 'edit_kelas',
                'label'  => 'Kelas',
                'rules'  => 'required|trim|min_length[2]|max_length[12]|is_unique[kelas.kelas]',
                'errors' => array(
                        'is_unique'     => 'Kelas telah terdaftar'
                ),
        )
        );
        $this->form_validation->set_rules($config);
        $this->form_validation->set_rules($config);
        if( ! $this->form_validation->run()){
            $error = validation_errors();
            $return['errors'] = $error;
        }else{
		$data = array(
		'kelas' => $this->input->post('edit_kelas'),
		);
		$data = $this->security->xss_clean($data);
		$update = $this->M_kelas->update(array('id' => $this->input->post('edit_id')), $data);
        $return['status'] = $update['res'];
        }
		echo json_encode($return);
	}

	public function delete_json(){
		$id = $this->input->post('id');
		$delete = $this->M_kelas->delete($id);
		$return['status'] = $delete['res'];
		echo json_encode($return);
	}

      public function get_by_id($id)
      {
        $res = $this->M_kelas->get_by_id($id);
        echo json_encode($res);
      }

      public function export_excel(){
		$data = array( 'title' => 'Laporan Kelas | Digital Academy',
		'kelas' => $this->M_kelas->getall());
		$this->load->view('admin/kelas/laporan_excel',$data);
	}
}

/* End of file KelasController.php */
/* Location: ./application/controllers/Admin/KelasController.php */
