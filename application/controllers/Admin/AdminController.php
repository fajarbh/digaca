<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends MY_Controller {

	public function index()
	{
		$title['nav_title'] = 'Dashboard';
		$this->load->view('frontend/V_header',$title);
		$data['jml_murid'] = $this->M_murid->get_jml_murid();
		$this->load->view('admin/V_dashboard',$data);
		$this->load->view('frontend/V_footer');
	}

	public function kelas()
	{
		$title['nav_title'] = 'Kelas';
		$this->load->view('frontend/V_header',$title);
		$this->load->view('admin/kelas/V_kelas');
		$this->load->view('admin/kelas/addjs');
		$this->load->view('frontend/V_footer');
	}

	public function guru()
	{
		$this->load->view('frontend/V_header',$title);
		$this->load->view('admin/V_guru');
		$this->load->view('frontend/V_footer');
	}

	public function mapel()
	{
		$title['nav_title'] = 'Mapel';
		$this->load->view('frontend/V_header',$title);
		$this->load->view('admin/mapel/V_mapel');
		$this->load->view('admin/mapel/addjs');
		$this->load->view('frontend/V_footer');
	}

	public function murid()
	{
		$title['nav_title'] = 'Murid';

		$data ['kelas'] = $this->M_kelas->getAllData()->result();
		$data ['thn_ajaran'] = $this->M_admin->getAllthnAjaran();

		$this->load->view('frontend/V_header',$title);
		$this->load->view('admin/Siswa/V_siswa',$data);
		$this->load->view('admin/Siswa/addjs');
		$this->load->view('frontend/V_footer');
	}

	public function akunadmin()
	{
		$title['nav_title'] = 'Akun Admin';
		$this->load->view('frontend/V_header',$title);
		$this->load->view('admin/V_akunadmin');
		$this->load->view('frontend/V_footer');
	}

	public function akunguru()
	{

	}

	public function akunwalikelas()
	{

	}

	public function akunsiswa()
	{

	}



}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin/Admin.php */
