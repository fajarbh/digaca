<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MapelController extends MY_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('M_mapel');
        }

        public function mapel_list_json()
        {
                $datamapel = $this->M_mapel->getAllData()->result();
                $no = 1;
                foreach ($datamapel as $mapel) {
                        $tbody = array();
                        $tbody[] = $no++;
                        $tbody[] = $mapel->kode_mapel;
                        $tbody[] = $mapel->nama_mapel;
                        $aksi    = '<a class="" href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_mapel('."'".$mapel->id."'".')"><i class="fa fa-check text-primary"></i></a>
                        <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_mapel('."'".$mapel->id."'".')"><i class="fa fa-trash text-primary"></i></a>';
                        $tbody[] = $aksi;
                        $data[]  = $tbody;
                }
                if ($datamapel) {
                        echo json_encode(array('data' => $data));
                }else{
                        echo json_encode(array('data' => 0));
                }
        }

        public function add_json()
        {
                $config = array(
                        array(
                                'field'  => 'add_kode',
                                'label'  => 'Kode',
                                'rules'  => 'required|trim|min_length[2]|max_length[12]|is_unique[mapel.kode_mapel]',
                                'errors' => array(
                                        'is_unique'     => 'Kode Sudah Tersedia'
                                ),
                        ),
                        array(
                                'field'  => 'add_mapel',
                                'label'  => 'Mapel',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        )
                );
                $this->form_validation->set_rules($config);
                if( ! $this->form_validation->run()){
                        $error = validation_errors();
                        $return['errors'] = $error;
                }else{
                        $data = array(
                                'kode_mapel' => $this->input->post('add_kode'),
                                'nama_mapel' => $this->input->post('add_mapel'),
                        );
                        $data = $this->security->xss_clean($data);
                        $insert = $this->M_mapel->save($data);

                        $return['status'] = $insert['res'];
                }

                echo json_encode($return);
                exit;

        }

        public function edit_json()
        {

                $config = array(
                        array(
                                'field'  => 'edit_kode',
                                'label'  => 'Kode',
                                'rules'  => 'required|trim|min_length[2]|max_length[12]',
                        ),
                        array(
                                'field'  => 'edit_mapel',
                                'label'  => 'Mapel',
                                'rules'  => 'required|trim|min_length[2]|max_length[20]'
                        )
                );
                $this->form_validation->set_rules($config);
                $this->form_validation->set_rules($config);
                if( ! $this->form_validation->run()){
                        $error = validation_errors();
                        $return['errors'] = $error;
                }else{
                        $data = array(
                                'kode_mapel' => $this->input->post('edit_kode'),
                                'nama_mapel' => $this->input->post('edit_mapel'),
                        );
                        $update = $this->M_mapel->update(array('id' => $this->input->post('edit_id')), $data);
                        $return['status'] = $update['res'];
                }
                echo json_encode($return);
        }

        public function delete_json(){
                $id = $this->input->post('id');
                $delete = $this->M_mapel->delete($id);
                $return['status'] = $delete['res'];
                echo json_encode($return);
        }

        public function get_by_id($id)
        {
                $res = $this->M_mapel->get_by_id($id);
                echo json_encode($res);
        }
  }

/* End of file KelasController.php */
/* Location: ./application/controllers/Admin/KelasController.php */
