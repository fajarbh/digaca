<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginadminController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_auth');
  }


  public function message($title = NULL,$text = NULL,$type = NULL) {
    return $this->session->set_flashdata([
      'title' => $title,
      'text' => $text,
      'type' => $type
    ]
  );
  } 
  public function index()
  {		
    $this->load->view('admin/V_loginadmin');
  }

  public function checkToken() {
    if ($this->session->userdata('backToken')) {
      if ($this->M_auth->checkToken($this->session->userdata('backToken'))) {
        redirect('Siswa');
      } else {
        $this->session->unset_userdata('backToken');
        redirect('Siswa/logout');
      }
    }
  }


  public function login() {
    $data = $this->input->post();
    if ($this->input->post()) {
      $dataAdmin = $this->M_auth->getAdminByUsername($data['username']);
      $dataTeacher = $this->M_auth->getTeacherByUsername($data['username']);
      
      $cek_admin   = $dataAdmin->row_object();
      $cek_teacher = $dataTeacher->row_object();

      if ($cek_admin) {
      if ($dataAdmin->num_rows() == 0) {
        $this->message('Ooppsss','Username yang anda masukkan tidak terdaftar, silahkan coba lagi','error');
        redirect('Admin/login');
      }else{
        $row_data = $dataAdmin->row();
        $password = password_verify($data['password'],$row_data->password);
        if ($password == TRUE) 
        {
          $sess_ = [
            'full_name'   => $row_data->nama,
            'email'       => $row_data->email,
            'level'       => $row_data->id_role,
            'id_'         => $row_data->id_admin,
            'backToken'   => crypt($row_data->id_role,'')
          ];
          $this->session->set_userdata( $sess_ );
          $this->M_auth->registToken($forToken = ['access_token' => $sess_['backToken']]);
          $this->message('Selamat datang '.$row_data->nama.'!','Semoga hari anda menyenangkan:)','success');
          redirect('Admin');
        } else {
          $this->message('Ooppsss','Email dan password tidak sesuai, silahkan coba lagi','error');
            redirect('Admin/login');
          
        }
      }

    }elseif($cek_teacher) {
      if($dataTeacher->num_rows() == 0){
        $this->message('Ooppsss','Username yang anda masukkan tidak terdaftar, silahkan coba lagi','error');
      }else{
        $row_teacher = $dataTeacher->row();
        $password = password_verify($data['password'],$row_teacher->password);
        if ($password == TRUE)
        {
          $sess_ = [
            'nip'         => $row_teacher->nip,
            'full_name'   => $row_teacher->nama,
            'level'       => $row_teacher->id_role,
            'id_'         => $row_teacher->id_guru,
            'GuruToken'   => crypt($row_teacher->nip,'')
          ];
        $this->session->set_userdata( $sess_ );
        $this->message('Selamat datang '.$row_data->nama.'!','Semoga hari anda menyenangkan:)','success');
        redirect('Guru');
      } else {
        $this->message('Ooppsss','Email dan password tidak sesuai, silahkan coba lagi','error');
           redirect('Admin/login');
        
      }
    }
  } else {
    $this->message('Ooppsss','Username tidak terdaftar, silahkan coba username lain','error');
      redirect('Admin/login');
  }
      }
    $this->message('Ooppsss','Username tidak terdaftar, silahkan coba username lain','error');
        redirect('Admin/login');
    }
    


  public function logout()
  {
    $this->session->sess_destroy();
    redirect('Admin/login');
  }

}

/* End of file LoginadminController.php */
/* Location: ./application/controllers/Admin/LoginadminController.php */