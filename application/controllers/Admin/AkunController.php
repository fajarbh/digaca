<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AkunController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_akun');
	}

	public function akun_list_json()
	{
		$list = $this->M_akun->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $akun) {
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $akun->nama;
			$row[] = $akun->email;
            //add html for action
            $row[] = '<a class=""href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_guru('."'".$akun->id."'".')"><i class="fa fa-check"></i></a>
                  	  <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_akun('."'".$akun->id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_akun->count_all(),
                        "recordsFiltered" => $this->M_akun->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
		
	}

    public function delete_json(){
        $id = $this->input->post('delete_id');
        $result = $this->M_akun->delete($id);
        echo json_encode($result);
    }

      public function get_by_id($id)
      {
        $data = $this->M_akun->get_by_id($id);
        echo json_encode($data);
      }

}

/* End of file AkunController.php */
/* Location: ./application/controllers/Admin/AkunController.php */