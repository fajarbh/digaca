<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuruController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_guru');
	}


	public function guru_list_json()
	{
        $list = $this->M_guru->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $guru) {
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $guru->nip;
			$row[] = $guru->nama;
			$row[] = $guru->agama;
			$row[] = $guru->jabatan;
			$row[] = $guru->tgl_lahir;
			$row[] = $guru->jk;
            //add html for action
            $row[] = '<a class="" href="javascript:void(0)" data-toggle="tooltip"  title="Edit" onclick="edit_guru('."'".$guru->id."'".')"><i class="fa fa-check"></i></a>
                  	  <a class="text-inverse" href="javascript:void(0)"  title="Delete" data-toggle="tooltip" onclick="delete_guru('."'".$guru->id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_guru->count_all(),
                        "recordsFiltered" => $this->M_guru->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function add_json()
	{
		date_default_timezone_set('ASIA/JAKARTA'); 
		$data = array(
		'nip'  	 	 => $this->input->post('add_nip'),
		'nama'   	 => $this->input->post('add_nama'),
		'jabatan'	 => $this->input->post('add_jabatan'),
		'alamat' 	 => $this->input->post('add_alamat'),
		'email'  	 => $this->input->post('add_email'),
		'agama'  	 => $this->input->post('add_agama'),
		'tgl_lahir'  => $this->input->post('add_tgl_lahir'),
		'jk'  	 	 => $this->input->post('add_jk'),
		);
		$insert = $this->M_guru->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function edit_json()
	{
		date_default_timezone_set('ASIA/JAKARTA'); 
		$data = array(
			'nip' 		 => $this->input->post('edit_nip'),
			'nama' 	 	 => $this->input->post('edit_nama'),
			'jabatan' 	 => $this->input->post('edit_jabatan'),
			'alamat' 	 => $this->input->post('edit_alamat'),
			'email' 	 => $this->input->post('edit_email'),
			'agama'  	 => $this->input->post('edit_agama'),
			'tgl_lahir'  => $this->input->post('edit_tgl_lahir'),
			'jk'  	 	 => $this->input->post('edit_jk'),
		);
		$this->M_guru->update(array('id' => $this->input->post('edit_id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function delete_json(){
		$id = $this->input->post('delete_id');
		$result = $this->M_guru->delete($id);
		echo json_encode($result);
	}

      public function get_by_id($id)
      {
        $data = $this->M_guru->get_by_id($id);
        echo json_encode($data);
      }



}

/* End of file guruController.php */
/* Location: ./application/controllers/Admin/guruController.php */
