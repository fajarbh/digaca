<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class SiswaController extends CI_Controller {

	        public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('google');
        $this->load->library('facebook');
        if (!$this->session->userdata('userToken')) {
			$this->session->sess_destroy();
			redirect('Siswa/login');
		}
    }

      public function message($title = NULL,$text = NULL,$type = NULL) {
        return $this->session->set_flashdata([
                'title' => $title,
                'text' => $text,
                'type' => $type
            ]
        );
    }


	public function index()
	{
		$data['logoutURL'] = $this->facebook->logout_url();
		$data['userData'] = $this->session->userdata('userData');
		$this->load->view('siswa/V_header',$data);
		$this->load->view('siswa/V_dashboard');
		$this->load->view('siswa/V_footer');
	}

		public function logout() {
		   $this->session->unset_userdata('UserToken');
 /*       $this->google->revokeToken($_SESSION['access_token']);*/
        $this->facebook->destroy_session();
        $this->session->sess_destroy();
        unset($_SESSION['access_token']);
        $userData=array(
            'is_login'=>false);
        $this->session->set_userdata($userData);
        redirect('Siswa/login','refresh');
	}

	

}

/* End of file SiswaController.php */
/* Location: ./application/controllers/Siswa/SiswaController.php */