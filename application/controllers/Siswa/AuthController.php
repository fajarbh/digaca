<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('facebook');
        $this->load->library('google');
        $this->load->model('M_auth');
        $this->load->model('M_user');
    }

        public function message($title = NULL,$text = NULL,$type = NULL) {
        return $this->session->set_flashdata([
                'title' => $title,
                'text' => $text,
                'type' => $type
            ]
        );
    }

    public function checkToken() {
        if ($this->session->userdata('backToken')) {
            if ($this->M_auth->checkToken($this->session->userdata('backToken'))) {
                redirect('Siswa');
            } else {
                $this->session->unset_userdata('backToken');
                redirect('Siswa/logout');
            }
        }
    }

        public function index() {
           $userData = array();
// Check if user is logged in
        if($this->facebook->is_authenticated()){
// Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

// Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']      = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']     = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']      = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']          = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['picture']        = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']           = !empty($fbUser['link'])?$fbUser['link']:'';
// Insert or update user data
            $userID = $this->M_user->checkUser($userData);
            $sess_data = array(
                'oauth_provider' => 'facebook',
                'id_role'     => "25",
                'userToken'   => crypt($fbUser['first_name'],'')
            );
// Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata('userData', $userData);
                $this->session->set_userdata($sess_data);
            }else{
                $data['userData'] = array();
            }

// Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
// Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }

        if ($google_data=$this->google->validate()) {
            $userData=array(
                'oauth_provider'=>'google',
                'oauth_uid'=>$google_data['oauth_uid'],
                'first_name'=>$google_data['first_name'],                
                'last_name'=>$google_data['last_name'],                
                'email'=>$google_data['email'],
                'picture'=>$google_data['picture'],                                                                                
                'link'=>$google_data['link'],                                                                                
            );
            $userID = $this->M_user->checkUser($userData);
            $sess_data = array(
                'id_role'         => "25",
                'userToken'       => crypt($google['first_name'],'')
            );
// Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata($userData);
                $this->session->set_userdata($sess_data);
            }else{
                $data['userData'] = array();
            }
        }

//melakukan pengalihan halaman sesuai dengan levelnya
        if ($this->session->userdata('userToken')) {
            redirect('Siswa');
        }
        $data['authURL'] =  $this->facebook->login_url();
        $data['loginURL'] = $this->google->get_login_url();
// Load google login view
        $this->load->view('siswa/V_loginsiswa',$data);
    }


    public function login() {
        $data = $this->input->post();
        if ($this->input->post()) {
            $dataUser = $this->M_auth->getUserByEmail($data['email']);
            if ($dataUser->num_rows() == 0) {
                    $this->message('Ooppsss','Email yang anda masukkan tidak terdaftar, silahkan coba lagi','error');
                    redirect('Siswa/login');
                }else{
                    $row_data = $dataUser->row();
                    $password = password_verify($data['password'],$row_data->password);
                if ($password == TRUE)
                {
                    $sess_ = [
                        'nis'         => $row_data->nis,
                        'full_name'   => $row_data->nama,
                        'email'       => $row_data->email,
                        'level'       => $row_data->id_role,
                        'id_'         => $row_data->id_user,
                        'userToken'   => crypt($row_data->nama,'')
                    ];
                    $this->session->set_userdata( $sess_ );
                    $this->message('Selamat datang '.$row_data->nama.'!','Semoga hari anda menyenangkan:)','success');
                    redirect('Siswa');
                } else {
                    $this->message('Ooppsss','Email dan password tidak sesuai, silahkan coba lagi','error');
                    redirect('Siswa/login');
                }
            }
        }
    }


    public function check_register()
    {
        $this->form_validation->set_rules('nis','Nis','trim|required|min_length[5]|max_length[20]');
        $this->form_validation->set_rules('email','Email','trim|required|min_length[5]|max_length[50]|is_unique[users.email]',
            array('is_unique' => 'Email yang kamu masukkan telah terdaftar,silahkan daftar memakai email yang belum terdaftar')
        );
        $this->form_validation->set_rules('nama','Nama','trim|required|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('password','Password','trim|required|min_length[6]|max_length[20]');
        if (! $this->form_validation->run()) {
            $error = validation_errors();
            $this->session->set_flashdata('message', '<div class="alert bg-warning" role="alert">'.$error.'</div>');
            redirect('Siswa/login');
        } else {
        $data_nis = array('nis' => $this->input->post('nis'));

        $row      = $this->M_auth->selectBy('murid',$data_nis);
        if ($row->num_rows() == 0)

        $row      = $this->M_auth->selectBy('murid',$data_nis);
        if ($row->num_rows() == 0) 
        {
            $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">NIS tidak terdaftar, silahkan coba lagi</div>');
            redirect('Siswa/login');

        }else{


            $email = $this->input->post('email', true);
            $data = [
                'nis'       => $this->input->post('nis'),
                'nama'      => htmlspecialchars($this->input->post('name', true)),
                'email'     => htmlspecialchars($email),
                'password'  => get_hash($this->input->post('password')),
                'id_role'   => 25,
                'is_active' => 0

            ];
            $token = base64_encode(random_bytes(32));
            $user_token = [
                'email' => $email,
                'token' => $token,
                'created_at' => time()
            ];
            $this->db->insert('users', $data);
            $this->db->insert('user_token', $user_token);
            $this->_sendEmail($token,'verify');
            $this->session->set_flashdata('message', '<div class="alert bg-success text-white" role="alert">Berhasil Membuat akun.Silahkan cek email untuk memverifikasi</div>');
            redirect('Siswa/login');
            }
        }
    }

    private function _sendEmail($token, $type)
    {
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'digacalogin@gmail.com',
            'smtp_pass' => 'digaca1234',
            'smtp_port' =>  465,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];
        $this->load->library('email',$config);
        $this->email->initialize($config);
        $this->email->from('digacalogin@gmail.com', 'Digital Academy');
        $this->email->to($this->input->post('email'));
        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            $this->email->message('Click This link to verify you acount : <a href="' . base_url() . 'Siswa/AuthController/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
        }else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click This link to reset your password : <a href="' . base_url() . 'Siswa/AuthController/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }


        if($this->email->send()) {
            return true;
        }else{
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');
        $user = $this->db->get_where('users', ['email' => $email])->row_array();        
        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token'=> $token])->row_array();
            if ($user_token) {
                if (time() - $user_token['created_at'] < (60*60*24)) {
                    $this->db->set('is_active', 1);
                    $this->db->where('email', $email);
                    $this->db->update('users');
                    $this->db->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('message', '<div class="alert bg-success text-white" role="alert">'. $email .' Akun telah aktif,silahkan login</div>');
                    redirect('Siswa/login');
                }else{
                    $this->db->delete('users', ['email' => $email]);
                    $this->db->delete('user_token', ['email' => $email]);

                    $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Account activation failed! Token expired.</div>');
                    redirect('Siswa/login');
                }
            }else{
                
                $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Account activation failed! Wrong token.</div>');
                redirect('Siswa/login');
            }
        }else{
            $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Account activation failed! Wrong email.</div>');
            redirect('Siswa/login');
        }
    }

    public function forgotpassword()
    {
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');

        if ($this->form_validation->run()==false) {
        $this->load->view('Siswa/V_forgotpass');
    }else {
        $email = $this->input->post('email');
        $user = $this->db->get_where('users',['email' => $email, 'is_active' => 1])->row_array();
            if ($user) {
                $token = base64_encode(random_bytes(32));
                $user_token = [
                    'email' => $email,
                    'token' => $token,
                    'created_at' => time()
                ];
                $this->db->insert('user_token',$user_token);
                $this->_sendEmail($token, 'forgot');
                $this->session->set_flashdata('message', '<div class="alert bg-success text-white" role="alert">Please check your email.</div>');
                redirect('Siswa/forgotpassword');
            }else {
                $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Email is not registered or not activated.</div>');
                redirect('Siswa/forgotpassword');
            }

        }
    }

    public function resetpassword()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');
        $user = $this->db->get_where('users', ['email'=>$email])->row_array();
        $user_token = $this->db->get_where('user_token', ['token'=>$token])->row_array();        
        if ($user) {
            if ($user_token) {
                $this->session->set_userdata('reset_email', $email);
                $this->changePassword();
            }else {
                $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Reset password failed. Wrong token !</div>');
                redirect('Siswa/login');
            }
        }else {
            $this->session->set_flashdata('message', '<div class="alert bg-danger text-white" role="alert">Reset password failed. Wrong email !</div>');
            redirect('Siswa/login');
        }
    }

    public function changePassword()
    {
        if (!$this->session->userdata('reset_email')) {
            redirect('Siswa/login');
        }
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|max_length[24]|matches[password2]');
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|max_length[24]|matches[password1]');
        if ($this->form_validation->run()==false) {            
        $this->load->view('Siswa/V_changepass');
        }else{
            $password = get_hash($this->input->post('password1'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');
            $this->db->set('password',  $password);
            $this->db->where('email', $email);
            $this->db->update('users');

            $this->session->unset_userdata('reset_email');
            $this->session->set_flashdata('message', '<div class="alert bg-success text-white" role="alert">Reset password success !</div>');
            redirect('Siswa/login');
        }
    }

        public function logout() {
    $this->session->unset_userdata('backToken');
 /*       $this->google->revokeToken($_SESSION['access_token']);*/
        $this->facebook->destroy_session();
        $this->session->sess_destroy();
        unset($_SESSION['access_token']);
        $userData=array(
            'is_login'=>false);
        $this->session->set_userdata($userData);
        redirect('Siswa/login','refresh');
    }


}

/* End of file Auth.php */
/* Location: ./application/controllers/Siswa/Auth.php */
