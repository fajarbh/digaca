<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OauthController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('facebook');
        $this->load->library('google');
        $this->load->model('M_user');
    }


    public function index(){
        $userData = array();

// Check if user is logged in
        if($this->facebook->is_authenticated()){
// Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

// Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']      = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']     = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']      = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']          = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['picture']        = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']           = !empty($fbUser['link'])?$fbUser['link']:'';
// Insert or update user data
            $userID = $this->M_user->checkUser($userData);
            $sess_data = array(
                'oauth_provider' => 'facebook',
                'id_role'     => "25",
                'userToken'   => crypt($fbUser['first_name'],'')
            );
// Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata('userData', $userData);
                $this->session->set_userdata($sess_data);
            }else{
                $data['userData'] = array();
            }

// Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
// Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }

        if ($google_data=$this->google->validate()) {
            $userData=array(
                'oauth_provider'=>'google',
                'oauth_uid'=>$google_data['oauth_uid'],
                'first_name'=>$google_data['first_name'],                
                'last_name'=>$google_data['last_name'],                
                'email'=>$google_data['email'],
                'picture'=>$google_data['picture'],                                                                                
                'link'=>$google_data['link'],                                                                                
            );
            $userID = $this->M_user->checkUser($userData);
            $sess_data = array(
                'id_role'         => "25",
                'userToken'       => crypt($google['first_name'],'')
            );
// Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata($userData);
                $this->session->set_userdata($sess_data);
            }else{
                $data['userData'] = array();
            }
        }

//melakukan pengalihan halaman sesuai dengan levelnya
        if ($this->session->userdata('userToken')) {
            redirect('Siswa');
        }

        $data['loginURL'] = $this->google->get_login_url();
// Load google login view
        $this->load->view('siswa/V_loginsiswa',$data);
    }






}