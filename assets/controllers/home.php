<?php 
 defined('BASEPATH') OR exit ('No direct script access allowed');

 
 class Home extends CI_Controller
 {
 	/**
		*author     : Shafa Ardila
		*Email      : shafaardila123@gmail.com
		*Keterangan : fungsi yang pertama kali dipanggil
		*Tanggal    : 26 Jul 2019
	*/
 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model('siswa_model');
 	}
 




	public function index()
	{
		$data['pesan_login'] = 'Jika sudah punya akun,Silahkan Login';
		$data['pesan_register'] = 'Jika sudah punya akun,Silahkan Register';
		$data['siswa'] = array(
						'nis'  => '12345',
						'nama' => 'shafa');
		$data['siswa_rpl'] = array(
							array(
							'Nis'  => '1234',
							'Nama' => 'Anggun',
							'jk'   => 'Perempuan'
							 ),
							array(
							'Nis'  => '123',
							'Nama' => 'Agus',
							'jk'   => 'Laki-Laki'
							),
							array(
							'Nis'  => '12',
							'Nama' => 'Fauzan',
							'jk'   => 'Laki-laki')
							
							);
		$data['data_siswa']=$this->siswa_model->selectAllData()->result();
		//var_dump($data['data_siswa']);exit;

		$this->load->view('home_v', $data);
	}
}
?>
