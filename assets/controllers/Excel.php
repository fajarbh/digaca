<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel extends CI_Controller {

// Load database
 public function __construct() {
 parent::__construct();
 $this->load->model('siswa_model');
 }

public function export_excel(){
 $data = array( 'title' => 'Laporan Excel',
 'siswa' => $this->siswa_model->selectAllData('siswa')->result());
 $this->load->view('laporan_excel',$data);
 }

}

/* End of file Excel.php */
/* Location: ./application/controllers/Excel.php */