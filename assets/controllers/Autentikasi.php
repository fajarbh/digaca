<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autentikasi extends CI_Controller {

	/**
		*author     : Shafa Ardila
		*Email      : shafaardila123@gmail.com
		*Keterangan : fungsi yang pertama kali dipanggil
		*Tanggal    : 27 Agustus 2019
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model');
	}


	public function index()
	{
		//numeric random number for captcha
		$random_number = substr(number_format(time()* rand(), 0, '',''), 0, 6);
		//captcha configuration
		$config = array(
			'word' => $random_number,
			'img_path' => 'assets/captcha_images/',
			'img_url'  => base_url() . 'assets/captcha_images/',
			'img_width' => '150',
			'img_height' => 50,
			'font_size' => 16
			);
		$captcha = create_captcha($config);

		//unset previous captcha and store new captcha 
		$this->session->unset_userdata('captchaCode');
		$this->session->set_userdata('captchaCode', $captcha['word']);

		//send captcha images to view
		$data['captchaImg'] = $captcha['image'];
		$this->load->view('login_v', $data);
	}
	public function refreshCaptcha()
	{
		//numeric random number for captcha
		$random_number = substr(number_format(time()* rand(), 0, '',''), 0, 6);
		//captcha configuration
		$config = array(
			'word' => $random_number,
			'img_path' => 'assets/captcha_images/',
			'img_url'  => base_url() . 'assets/captcha_images/',
			'img_width' => '150',
			'img_height' => 50,
			'font_size' => 16
			);
		$captcha = create_captcha($config);

		//unset previous captcha and store new captcha 
		$this->session->unset_userdata('captchaCode');
		$this->session->set_userdata('captchaCode', $captcha['word']);

		//display captcha image
		echo $captcha['image'];
	}
	/**
		*author     : Shafa Ardila
		*Email      : shafaardila123@gmail.com
		*Keterangan : fungsi yang pertama kali dipanggil
		*Tanggal    : 3 September 2019
	*/
	public function verifikasi()
	{
		$inputCaptcha = $this->input->post('captcha');
		$sessCaptcha  = $this->session->userdata('captchaCode');
		$nis		  = $this->input->post('nis');
		$password	  = $this->input->post('password');

		if ($inputCaptcha !== $sessCaptcha) {
			echo "Captcha salah";
		}else{

			$data_nis = array('nis' => $nis);
			$row	  = $this->siswa_model->selectBy('siswa',$data_nis);


			if($row->num_rows() == 0)
			{
				echo "Nis tidak ditemukan";
		}else {

		$row_data = $row->row();

		$password = decrypt($row_data->password,$password);
	
		if ($password == TRUE)
		{
			$data_user = array(
						 'nis' 		 => $row_data->nis,
						 'nama'		 => $row_data->nama,
						 'logged_in' => TRUE
			);

			$this->session->set_userdata($data_user);
			redirect('siswa/homepage');
		}else{
			echo "Password yang Anda Masukan Salah";
		}
		}
		}
	}
	/**
		*author     : Shafa Ardila
		*Email      : shafaardila123@gmail.com
		*Keterangan : fungsi yang pertama kali dipanggil
		*Tanggal    : 3 September 2019
	*/
	public function logout(){
		$this->session->sess_destroy();
		redirect('Autentikasi');	
	}
}
