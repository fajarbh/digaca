<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	/**
	 *author     : Diva Wardana
	 *Email      : divawardana26@gmail.com
	 *Date 		 : 27 Agustus 2019
	 *Ket        : Function yang pertama kali dipanggil di ci class siswa
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model');
	}

	/**
	 *author     : Diva Wardana
	 *Email      : divawardana26@gmail.com
	 *Date 		 : 21 Agustus 2019
	 *Ket        : Function yang pertama kali dipanggil di ci class siswa
	*/
	public function index()
	{
		$data['siswa'] = $this->siswa_model->selectAllData('siswa')->result();
		$data['jml_jurusan'] = $this->siswa_model->get_jml_jurusan();
		//var_dump($data['data_dr_db']);exit;untuk mengecek yg error
		$this->load->view('siswa_v', $data);
	}
	public function home()
	{
		//echo encrypt('Adm1n!@');
		//echo decrypt('w8DCPow/TCo8+TQ6z3i/JQ==', 'Adm1n!@');
		//$this->load->view('homepage_v');

		$this->load->view('login_v');
	}
	public function registrasi()
	{
		//echo encrypt('Adm1n!@');
		//echo decrypt('w8DCPow/TCo8+TQ6z3i/JQ==', 'Adm1n!@');
		//$this->load->view('homepage_v');

		$this->load->view('registrasi_v');
	}
	/**
	 *author     : Diva Wardana
	 *Email      : divawardana26@gmail.com
	 *Date 		 : 27 Agustus 2019
	 *Ket        : fungsi untuk proses registrasi.
	*/
	public function registrasiAction()
	{
		$this->form_validation->set_rules('nis','nis','trim|required');
		$this->form_validation->set_rules('nama','nama','trim|required');
		$this->form_validation->set_rules('jk','jk','trim|required');
		$this->form_validation->set_rules('jurusan','jurusan','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');

		$nis		= $this->input->post('nis');
		$nama		= $this->input->post('nama');
		$jk			= $this->input->post('jk');
		$jurusan	= $this->input->post('jurusan');
		$password	= $this->input->post('password');

		if ($this->form_validation->run() == FALSE) {
			echo "Data yang Anda Masukan Salah";
		}
		else
		{
			$data_nis = array('nis' => $nis);
			$row	  = $this->siswa_model->selectBy('siswa',$data_nis);

			if ($row->num_rows() > 0) {
				echo "Nis Sudah Terdaftar";
			} else {
				$data_input = array(
							  'nis'		=> $nis,
							  'nama'	=> $nama,
							  'jk'		=> $jk,
							  'jurusan' => $jurusan,
							  'password'=> encrypt($password)
							  );
				$insert_query = $this->siswa_model->insert('siswa',$data_input);

				if ($insert_query == TRUE) {
					echo "Registrasi Berhasil";
				}else{
					echo "Registrasi Gagal";
				}
			}
		}
	}
	/**
	 *author     : Diva Wardana
	 *Email      : divawardana26@gmail.com
	 *Date 		 : 27 Agustus 2019
	 *Ket        : fungsi untuk Halaman User ketika sudah login.
	*/

	public function homepage()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['siswa'] = $this->siswa_model->selectAllData('siswa')->result();
			$data['jml_jurusan'] = $this->siswa_model->get_jml_jurusan();
			$this->load->view('home_v',$data);
		} else {
			redirect('autentikasi');
		}
		
	}
	/**
	 *author     : Diva Wardana
	 *Email      : divawardana26@gmail.com
	 *Date 		 : 27 Agustus 2019
	 *Ket        : fungsi untuk mengambil data siswa agar bisa ditampilkan menggunkana ajax.
	*/
	public function datasiswa()
	{
		$datasiswa = $this->siswa_model->selectAllData('siswa')->result_array();
		$no = 1;
		foreach ($datasiswa as $key) {
			$tbody = array();

			$tbody[] = $no++;
			$tbody[] = $value['nis'];
			$tbody[] = $value['nama'];
			$tbody[] = $value['jk'];
			$tbody[] = $value['jurusan'];
			$aksi= "<button class='btn btn-success ubah-siswa' data-toggle='modal' data-id=".$value['id'].">Ubah</button>".' '."<button class='btn btn-danger hapus-siswa' id='id' data-toggle='modal' data-id=".$value['id'].">Hapus</button>";
			$tbody[] = $aksi;
			$data[]	 = $tbody;
		}
		if ($datasiswa) {
			echo json_encode(array('data' => $data));
		}else{
			echo json_encode(array('data'=>0));
		}
	}
	public function tambahdata()
	{
		//didapat dari ajax yang dimana data
		$nis		= $this->input->post('nis');
		$nama		= $this->input->post('nama');
		$jk			= $this->input->post('jk');
		$jurusan	= $this->input->post('jurusan');
		$password	= $this->input->post('password');

		$data_nis 	= array('nis' => $nis);
		$row  		= $this->siswa_model->selectBy('siswa',$data_nis);
		if ($row->num_rows() > 0) 
		{
			$data = array('error' => 'Nis Sudah Terdaftar');
			echo json_encode($data);
		} else {
			$tambahdata = array(
				'nis'    	=> $nis,
				'nama'    	=> $nama,
				'jk'   		 => $jk,
				'jurusan'    => $jurusan,
				'password'    => $password
				);
			$data = $this->siswa_model->insert('siswa', $tambahdata);
			$data = array('status' => 1);
			echo json_encode($data);
		}
	}

	public function formedit()
	{
		// id yang telah diparsing pada ajax ajaxcrud.php data{1d:id}
		$id = array('id' => $this->input->post('id'));
		$data['datasiswa'] = $this->siswa_model->selectBy('siswa', $id)->row();
		$this->load->view('formubahsiswa_v',$data);
	}

	public function ubahsiswa()
	{
		$data = array(
			'nis'	=> $this->input->post('nis'),
			'nama'	=> $this->input->post('nama'),
			'jk'	=> $this->input->post('jk'),
			'jurusan'	=> $this->input->post('jurusan'),
			'password'	=> encrypt($this->input->post('password'))
		);

		$id = array('id' => $this->input->post('id'));
		$data = $this->siswa_model->update('siswa', $data, $id);

		echo json_encode($data);
	}

	public function hapusdata($id)
	{
		$where_id = array('id' => $id);
		$data = $this->siswa_model->delete('siswa', $where_id);

		if ($data) 
		{
			$data = array('msg'=>"Berhasil menghapus data");
			echo json_encode($data);
		} else {
			$data = array('msg' => "Gagal menghapus data");
			echo json_encode($data);
		}
	}
}
