<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pdfku extends CI_Controller {
   public function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    public function index(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'Data Siswa',0,1,'C');
        // $pdf->SetFont('Arial','B',12);
        // $pdf->Cell(190,7,'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'id',1,0);
        $pdf->Cell(85,6,'NIS',1,0);
        $pdf->Cell(27,6,'Nama Siswa',1,0);
        $pdf->Cell(25,6,'Jurusan',1,1);
        $pdf->SetFont('Arial','',10);
        $siswa = $this->db->get('tbl_siswa')->result();
        foreach ($siswa as $row){
            $pdf->Cell(20,6,$row->id,1,0);
            $pdf->Cell(85,6,$row->nis,1,0);
            $pdf->Cell(27,6,$row->nama_siswa,1,0);
            $pdf->Cell(25,6,$row->jurusan,1,1); 
        }
        $pdf->Output();
    }

    public function test(){
    	echo "string";
    }
}