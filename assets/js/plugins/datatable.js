    (function ($) {
    "use strict";

  var colors = ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown', 'blue-grey', 'grey'];
  
  var tabel = function(){
    $('#datatable').dataTable({
      ajax: '../assets/api/datatable.json',
      processing: true,
      columns: [
          { data: "name",
          },
          { data: "hr.position" },
          { data: "contact.0" },
          { data: "contact.1" },
          { data: "hr.start_date" },
          { data: "hr.salary" }
      ]
    });
  }

  // for ajax to init again
  $.fn.dataTable.init = tabel;

})(jQuery);
